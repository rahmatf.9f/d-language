import React, { useState,useEffect } from "react";
import { Switch, Route, useLocation } from "react-router-dom";
import MyClass from "./pages/userPage/myclass";
import ClassListMenu from "./pages/HomePage/ClassListMenu";
import NavbarAfterLogin from "./components/user/navbar";
import Footer from "./components/footer";
import DetailClass from "./pages/HomePage/detailClass";
import Checkout from "./pages/userPage/Checkout";
import { Box } from "@mui/material";
import Invoice from "./pages/userPage/invoice";
import InvoiceDetails from "./pages/userPage/invoice/details";
import LandingPage from "./pages/HomePage/LandingPage";
import Navbar from "./components/homepage/navbar";
import Login from "./pages/HomePage/LoginPage/Login";
import Register from "./pages/HomePage/RegisterPage/Register";
import EmailConfimationSuccess from './pages/HomePage/EmailConfirmationSuccess/EmailConfirm';
import ResetPassword from "./pages/HomePage/ResetPasswordPage/ResetPassword";
import { UserLogin } from './context/UserLogin';
import UserDashboard from "./pages/AdminPage/Dashboard";
import PaymentMenu from "./pages/AdminPage/Payment";
import InvoiceMenu from "./pages/AdminPage/Invoice";
import NewPassword from "./pages/HomePage/NewPasswordPage/NewPassword";
import SuccessPurcases from "./pages/HomePage/SuccessPurcases";

function App() {
  // let location = useLocation();
  let location = useLocation();
  let excludedRoutesFooter = ["/checkout", "/login", "/register", "/reset-password","/admin","/admin/payment","/admin/invoice","/new-password","/email-confirm","/success-purcase"];
  let excludedRoutesAppbar = ["/admin","/admin/payment","/admin/invoice","/email-confirm","/success-purcase"];
  const [loggedIn, setLoggedIn] = useState(false);
  const [userRole, setUserRole] = useState("");
  const [user,setuser] = useState({});
  // const hideFooterOnPages = ['/checkout']; // Specify the paths where you want to hide the footer

  // const shouldHideFooter = hideFooterOnPages.includes(location.pathname);
  useEffect(() => {
    const loggedIn = localStorage.getItem('loggedIn');
    const userRole = localStorage.getItem('userRole');
    const user = localStorage.getItem('user_id');

    if (loggedIn === 'true' && userRole && user) {
      setLoggedIn(true);
      setUserRole(userRole);
      setuser(user);
    }
  }, []);


  return (
    <UserLogin.Provider value={{ loggedIn, setLoggedIn,userRole, setUserRole,user,setuser }}>
    <Box component="app">
      {loggedIn ? (
        !excludedRoutesAppbar.includes(location.pathname) && <NavbarAfterLogin/>
      ) : !excludedRoutesAppbar.includes(location.pathname) && <Navbar />}
        <Route exact path="/email-confirm">
          <EmailConfimationSuccess />
        </Route>
        <Route exact path="/success-purcase">
          <SuccessPurcases/>
        </Route>
      <Switch>
        <Route exact path="/">
          <LandingPage />
        </Route>
        {loggedIn ? (
          userRole == "Admin" ? (
            <Route exact path="/admin">
            <UserDashboard/>
            </Route>
          ) : (
            <Route exact path="/">
              <LandingPage />
            </Route>
          )
        ) : (
          <Route exact path="/login" render={(props) => <Login {...props} setLoggedIn={setLoggedIn} setUserRole={setUserRole} />} />
        )}
        {loggedIn ? (
          userRole === "Admin" ? (
            <Route exact path="/admin/payment">
            <PaymentMenu/>
            </Route>
          ) : (
            <Route exact path="/">
              <LandingPage />
            </Route>
          )
        ) : (
          <Route exact path="/login" render={(props) => <Login {...props} setLoggedIn={setLoggedIn} setUserRole={setUserRole} />} />
        )}
        {loggedIn ? (
          userRole === "Admin" ? (
            <Route exact path="/admin/invoice">
            <InvoiceMenu/>
            </Route>
          ) : (
            <Route exact path="/">
              <LandingPage />
            </Route>
          )
        ) : (
          <Route exact path="/login" render={(props) => <Login {...props} setLoggedIn={setLoggedIn} setUserRole={setUserRole} />} />
        )}
        <Route exact path="/login" render={(props) => <Login {...props} setLoggedIn={setLoggedIn} setUserRole={setUserRole} />} />
        <Route exact path="/register">
          <Register />
        </Route>
        <Route exact path="/reset-password">
          <ResetPassword/>
        </Route>
        <Route exact path="/new-password">
          <NewPassword/>
        </Route>
        {loggedIn ? (
          <Route exact path="/myclass">
            <MyClass />
          </Route>
        ) : (
          <Route exact path="/login" render={(props) => <Login {...props} setLoggedIn={setLoggedIn} setUserRole={setUserRole} />} />
        )}
        <Route exact path="/menulist/:name">
          <ClassListMenu />
        </Route>
        {loggedIn ? (
          <Route path="/checkout">
            <Checkout />
          </Route>
        ) : (
          <Route exact path="/login" render={(props) => <Login {...props} setLoggedIn={setLoggedIn} setUserRole={setUserRole} />} />
        )}
        <Route exact path="/detailclass/:id">
          <DetailClass />
        </Route>
        {loggedIn ? (
          <Route exact path="/invoice">
            <Invoice />
          </Route>
        ) : (
          <Route exact path="/login" render={(props) => <Login {...props} setLoggedIn={setLoggedIn} setUserRole={setUserRole} />} />
        )}
        {loggedIn ? (
            <Route exact path="/invoice/details/:id">
              <InvoiceDetails />
            </Route>
        ) : (
          <Route exact path="/login" render={(props) => <Login {...props} setLoggedIn={setLoggedIn} setUserRole={setUserRole} />} />
        )}
      </Switch>
      {!excludedRoutesFooter.includes(location.pathname) && <Footer />}
    </Box>
    </UserLogin.Provider>
  );
}

export default App;
