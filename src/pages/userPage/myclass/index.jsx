import { Box,Container } from "@mui/material";
import React from "react";
import ClassList from "../../../components/user/ClassList";
import LoadingScreen from "../../../components/loading";
import useAsycn from "../../../hooks/useAsync";

export default function MyClass() {
    const user_id = localStorage.getItem('user_id');
    const ApiUrl = `${process.env.REACT_APP_GET_COURSE_BY_ID}?id=${user_id}`;

    const dataClass = useAsycn(ApiUrl);

    return(
        <Box component="main">
            <main>
                <Box sx={{ paddingBottom:5,paddingTop:13 }}>
                    <Container>
                        {dataClass.length > 0 ? (dataClass.map((items) =>{
                            return(
                                <ClassList item={items} key={items.id}/>
                            )
                        })) : <LoadingScreen/>}
                    </Container>
                </Box>
            </main>
        </Box>
    )
}