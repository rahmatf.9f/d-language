import React, { useState } from "react";
import {
  Box,
  useTheme,
  useMediaQuery,
  Typography,
  Grid,
  Container,
} from "@mui/material";
import CustomSeparator from "../../../components/user/customSeparator";
import DataTable from "../../../components/user/DataTables";
import { useParams } from "react-router-dom/cjs/react-router-dom.min";
import useAsync from "../../../hooks/useAsync";

export default function InvoiceDetails(params) {
  const [includeActionColumn] = useState(false);
  const theme = useTheme();
  const isMobile = useMediaQuery(theme.breakpoints.down("md"));
  const {id} = useParams();
  
  const columns = [
    { field: "id", headerName: "No" },
    { field: "name_course", headerName: "Course Name" },
    { field: "languages", headerName: "Languages" },
    { field: "shedule", headerName: "Schedule" },
    { field: "price", headerName: "Price" },
  ];

  const rows = [
    {
      id: 1,
      name_course: "Basic English for Junior",
      languages: "English",
      shedule: "Friday, 29 July 2022",
      price: 400000,
    },
    {
      id: 1,
      name_course: "Japanese Course : Kanji",
      languages: "Japanese",
      shedule: "Saturday, 30 July 2022",
      price: 300000,
    },
  ];

  // const apiUrl = `http://localhost:7082/Invoice/${id}/details`;

  // const detailData = useAsync(apiUrl)
  // console.log(detailData);

  // const rowes = Array.isArray(detailData)
  // ? detailData.map((item, index) => ({
  //   ...item,
  //   id: index + 1,
  //   name_course: item.description,
  //   languages: item.name,
  //   shedule: new Date(item.schedule_Detail).toLocaleDateString("en-EN",{ weekday: "long",day: "numeric", month: "long", year: "numeric" }),
  //   price: item.price
  // })) : [];

  const apiUrl = `${process.env.REACT_APP_GET_DETAIL_INVOICE}${id}/details`;

  // Gunakan hook asinkron untuk memuat data
  const detailData= useAsync(apiUrl);

  // Memastikan detailData adalah array sebelum diproses
  const rowes = Array.isArray(detailData)
  ? detailData.map((item, index) => ({
    ...item,
    id: index + 1,
    name_course: item.description,
    languages: item.name,
    shedule: new Date(item.schedule_Detail).toLocaleDateString("en-EN",{ weekday: "long",day: "numeric", month: "long", year: "numeric" }),
    price: item.price
  })) : [];

  return (
    <Box component="main">
      <main>
        <Box
          sx={{
            paddingBottom: 5,
            paddingTop: 10,
            marginX: isMobile ? "10px" : "120px",
          }}
        >
          <CustomSeparator />
          <Box sx={{ marginBottom:'40px' }}>
            <Typography
              variant="h5"
              sx={{ fontFamily: "Montserrat", marginBottom: 1 }}
            >
              Details Invoice
            </Typography>
            <Box sx={{ flexGrow: 1 }}>
              <Grid container spacing={2}>
                <Grid item xs={6} md={2}>
                  <Typography
                    variant="body1"
                    sx={{ fontFamily: "Montserrat", fontSize: "18px" }}
                  >
                    No. Invoice :
                  </Typography>
                </Grid>
                <Grid item xs={4} md={6}>
                  <Typography
                    variant="body1"
                    color=""
                    sx={{ fontFamily: "Montserrat", fontSize: "18px" }}
                  >
                    {id}
                  </Typography>
                </Grid>
              </Grid>
            </Box>
            <Box sx={{ flexGrow: 1 }}>
              <Grid container spacing={3}>
                <Grid item xs={6} md={2} >
                  <Typography
                    variant="body1"
                    sx={{ fontFamily: "Montserrat", fontSize: "18px" }}
                  >
                    Date :
                  </Typography>
                </Grid>
                <Grid item xs={6} md={6} sx={{ display: "flex", justifyContent: "start" }}>
                  <Typography
                    variant="body1"
                    color=""
                    sx={{ fontFamily: "Montserrat", fontSize: "18px" }}
                  >
                    {/* {new Date(detailData[0].order_Date).toLocaleDateString("en-EN",{ 
                      weekday: "long",
                      day: "numeric",
                      month: "long", 
                      year: "numeric" })} */}
                    {rowes.length > 0 && rowes[0].shedule}
                  </Typography>
                </Grid>
                <Grid item ={12} md={4} sx={{ display: "flex", justifyContent: "end" }}>
                <Typography
                    variant="h6"
                    color=""
                    sx={{ fontFamily: "Montserrat", fontWeight:700,marginRight:5 }}
                  >
                    Total Price:
                  </Typography>
                  <Typography
                    variant="h6"
                    color=""
                    sx={{ fontFamily: "Montserrat",fontWeight:700 }}
                  >
                    IDR {rowes.length > 0 && rowes[0].total_price}
                  </Typography>
                </Grid>
              </Grid>
            </Box>
          </Box>

          <DataTable columns={columns} rows={rowes} includeActionColumn={includeActionColumn}/>
        </Box>
      </main>
    </Box>
  );
}
