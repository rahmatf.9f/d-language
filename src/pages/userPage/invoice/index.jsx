import React, { useContext, useState } from "react";
import {
  Box,
  useTheme,
  useMediaQuery,
  Typography,
} from "@mui/material";
import CustomSeparator from "../../../components/user/customSeparator";
import DataTable from "../../../components/user/DataTables";
import useAsycn from "../../../hooks/useAsync";
import { UserLogin } from "../../../context/UserLogin";
export default function Invoice() {
    const [includeActionColumn] = useState(true);
    const theme = useTheme();
    const isMobile = useMediaQuery(theme.breakpoints.down("md"));
    const {user} = useContext(UserLogin);
    
    const columns = [
    { field: "id", headerName: "No" },
    { field: "id_invoice", headerName: "No. Invoice"},
    { field: "date", headerName: "Date"},
    { field: "course_count",headerName: "Total Course"},
    { field: "total_price",headerName: "Total Price"},
  ];

  // const rows = [
  //   { id: 1, id_invoice: "DLA00001", date: "12 July 2023", course_count: 1,total_price:400000},
  //   { id: 2, id_invoice: "DLA00002", date: "12 July 2023", course_count: 2,total_price:400000 },
  //   { id: 3, id_invoice: "DLA00003", date: "12 July 2023", course_count: 4,total_price:400000 },
  //   { id: 4, id_invoice: "DLA00004", date: "12 July 2023", course_count: 3,total_price:400000 },
  //   { id: 5, id_invoice: "DLA00005", date: "12 July 2023", course_count: 2,total_price:400000 },
  // ];

  const ApiUrl = `${process.env.REACT_APP_GET_INVOICE}${user}`;

  const datainvoice = useAsycn(ApiUrl);

  const rows = Array.isArray(datainvoice)
  ? datainvoice.map((item, index) => ({
    ...item,
    id: index + 1,
    id_invoice: item.id_Invoice,
    date: new Date(item.order_Date).toLocaleDateString("id-ID",{ day: "numeric", month: "long", year: "numeric" }),
    course_count: item.total_Course,
    total_price: item.total_Price
  })) : [];

  return (
    <Box component="main">
      <main>
        <Box
          sx={{
            paddingBottom: 5,
            paddingTop: 10,
            marginX: isMobile ? "10px" : "120px",
          }}
        >
          <CustomSeparator />
          <Box>
            <Typography variant="h5" sx={{ fontFamily: "Montserrat",marginBottom:1 }}>
              Menu Invoice
            </Typography>
            <DataTable columns={columns} rows={rows} includeActionColumn={includeActionColumn} />
          </Box>
        </Box>
      </main>
    </Box>
  );
}
