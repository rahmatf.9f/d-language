import {
  Box,
  Container,
  useTheme,
  useMediaQuery,
  Checkbox,
  Typography,
} from "@mui/material";
import React, { useState, useEffect,useContext } from "react";
import useAsycn from "../../../hooks/useAsync";
import CardlistCheck from "../../../components/user/cardCheckout";
import FooterCheckout from "../../../components/user/footercheck";
import LoadingScreen from "../../../components/loading";
import ModalPayment from "../../../components/user/modalPayment";
import useFormatPrice from "../../../hooks/useFormatPrice";
import { UserLogin } from "../../../context/UserLogin";
import useUpdateStatus from "../../../hooks/useUpdateStatus";
import usePostUserCourse from "../../../hooks/usePostUserCourse"
import { useHistory } from "react-router-dom";

export default function Checkout() {
  const {user} = useContext(UserLogin);
  const theme = useTheme();
  const isMobile = useMediaQuery(theme.breakpoints.down("sm"));
  const history = useHistory();
  const ApiUrl =
    `${process.env.REACT_APP_GET_CART}?id=${user}`;

  const dataClass = useAsycn(ApiUrl);
  const [checkedItems, setCheckedItems] = useState([]);
  const [allChecked, setAllChecked] = useState(false);
  const [totalPrice, setTotalPrice] = useState(0);
  const [open, setOpen] = React.useState(false);
  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);
  

  const updateStatus = useUpdateStatus();
  const postUserCourse = usePostUserCourse();
  //handle untuk output payment jika sudah menekan tombol pay now
  const handlePayment = async (selectedPaymentMethod, itemsList) => {
    const arrayUser_Course = itemsList.map(
        (item) => (
            {
                fk_id_class: item.id_class,
                fk_id_schedule: item.id_schedule,
                fk_id_user: user
            }
        )
    );

    // untuk menaruh method payment jika telah menekan tombol paynow
    try {
        // method put update status cart
        await updateStatus.updateStatus(itemsList);

        const invoice = itemsList.map((item) => {
          
          return {
            Order_Date: new Date(),
            Total_Course: itemsList.length,
            Total_Price: itemsList.reduce((total, item) => total + item.price, 0),
            Id_Payment: selectedPaymentMethod,
            User_Id: user,
            Id_Course: item.id_class,
            Id_Schedule: item.id_schedule,
        }
        })

        console.log(invoice);

        // POST the invoice to the API
        const response = await fetch(process.env.REACT_APP_POST_INVOICE, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(invoice)
        });

        if (!response.ok) {
            throw new Error('Failed to create invoice');
        }

        await postUserCourse.postUserCourse(arrayUser_Course)

        setTimeout(() => {
            history.push("/success-purcase");
        }, 2500);
    } catch (error) {
        console.error("payment Error:", error)
    }
};
  
  useEffect(() => {
    if (allChecked) {
      setCheckedItems(dataClass);
      calculateTotalPrice(dataClass);
    } else {
      setCheckedItems([]);
      calculateTotalPrice([]);
    }
  }, [allChecked, dataClass]);

    //handling card when all check
  const handleCheckAllChange = (event) => {
    setAllChecked(event.target.checked);
  };

  //handling items when card is check
  const handleCheckedItem = (item, isChecked) => {
    if (isChecked) {
      setCheckedItems([...checkedItems, item]);
      calculateTotalPrice([...checkedItems, item]);
    } else {
      setCheckedItems(checkedItems.filter((checkedItem) => checkedItem.id !== item.id));
      calculateTotalPrice(checkedItems.filter((checkedItem) => checkedItem.id !== item.id));
    }
  };

  //calculation total price when it check
  const calculateTotalPrice = (items) =>{
    let total = 0;
    for(let i = 0; i < items.length; i++){
      total += items[i].price;
    }
    setTotalPrice(total);
  }
  
  return (
    <Box
      component="main"
      sx={{
        marginX: isMobile ? 0 : "auto",
        marginTop: "auto",
        marginBottom:'80px'
      }}
    >
      <main>
        <Box sx={{ paddingBottom: 5, paddingTop: 5 }}>
          <Container>
            <Box sx={{ display: "flex", alignItems: "left", marginY: 4, border: '1px 1px solid black' }}>
              <Checkbox
                checked={allChecked}
                onChange={handleCheckAllChange}
                indeterminate={
                  checkedItems.length > 0 &&
                  !allChecked &&
                  checkedItems.length !== dataClass.length
                }
                sx={{
                  height: "24px",
                  width: "24px",
                  alignItems: "center",
                  marginTop: "5px",
                  marginRight: "20px",
                  marginLeft: "10px",
                }}
              />
              <Typography sx={{ fontFamily: "Montserrat", fontSize: "24px" }}>
                Pilih Semua
              </Typography>
            </Box>
            {dataClass.length > 0 ? (
              dataClass.map((item) => (
                <Box>
                  <CardlistCheck
                    item={item}
                    handleCheckedItem={handleCheckedItem}
                    isCheckedAll={checkedItems.some(
                      (checkedItem) => checkedItem.id_class === item.id_class
                    )}
                  />
                </Box>
              ))
            ) : (
              <LoadingScreen/>
            )}
          </Container>
        </Box>
      </main>
      <FooterCheckout priceCount={useFormatPrice(totalPrice)} onclick={handleOpen} />
      <ModalPayment 
      open={open}
      handleClose={handleClose}
      handlePayment={handlePayment}
      itemsList={checkedItems}
      />
    </Box>
  );
}
