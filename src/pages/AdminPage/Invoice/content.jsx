import React,{useState} from 'react';
import AppBar from '@mui/material/AppBar';
import Toolbar from '@mui/material/Toolbar';
import Paper from '@mui/material/Paper';
import Grid from '@mui/material/Grid';
import TextField from '@mui/material/TextField';
import Tooltip from '@mui/material/Tooltip';
import IconButton from '@mui/material/IconButton';
import SearchIcon from '@mui/icons-material/Search';
import RefreshIcon from '@mui/icons-material/Refresh';
import InvoiceAllDataTable from '../../../components/admin/invoiceAllDataTable';
import useAsycn from '../../../hooks/useAsync';

export default function Content() {
  const columns = [
    { field: "id", headerName: "No" },
    { field: "id_Invoice", headerName: "No. Invoice" },
    { field: "name", headerName: "Username" },
    { field: "order_Date", headerName: "orderDate"},
    { field: "total_Course", headerName: "Total Course"},
    { field: "total_Price",headerName: "Total Price"},
    { field: "payname",headerName: "Payment Method"},
  ];

  const [searchText, setSearchText] = useState('');

  const apiUrl = process.env.REACT_APP_GET_ALL_INVOICE;

  const datainvoice = useAsycn(apiUrl);

  const filterData = Array.isArray(datainvoice)
  ? datainvoice.filter((row) => {
      return (
        row.name.toLowerCase().includes(searchText.toLowerCase()) ||
        row.id_Invoice.toLowerCase().includes(searchText.toLowerCase())
      );
    })
  : [];

  const rows = Array.isArray(filterData)
  ? filterData.map((item, index) => ({
    ...item,
    id: index + 1,
    id_Invoice: item.id_Invoice,
    name: item.name,
    order_Date: new Date(item.order_Date).toLocaleDateString("id-ID",{ day: "numeric", month: "long", year: "numeric" }),
    total_Course: item.total_Course,
    total_price: item.total_Price,
    payname: item.payname
  })) : [];
  
  return (
    <Paper sx={{ maxWidth: 1500, margin: 'auto', overflow: 'hidden' }}>
      <AppBar
        position="static"
        color="default"
        elevation={0}
        sx={{ borderBottom: '1px solid rgba(0, 0, 0, 0.12)' }}
      >
        <Toolbar>
          <Grid container spacing={2} alignItems="center">
            <Grid item>
              <SearchIcon color="inherit" sx={{ display: 'block' }} />
            </Grid>
            <Grid item xs>
              <TextField
                fullWidth
                placeholder="Search by Invoice UID or User Invoice"
                InputProps={{
                  disableUnderline: true,
                  sx: { fontSize: 'default' },
                }}
                variant="standard"
                value={searchText}
                onChange={(e)=> setSearchText(e.target.value)}
              />
            </Grid>
            <Grid item>
              <Tooltip title="Reload">
                <IconButton>
                  <RefreshIcon color="inherit" sx={{ display: 'block' }} />
                </IconButton>
              </Tooltip>
            </Grid>
          </Grid>
        </Toolbar>
      </AppBar>
      <InvoiceAllDataTable columns={columns} rows={rows}/>
    </Paper>
  );
}