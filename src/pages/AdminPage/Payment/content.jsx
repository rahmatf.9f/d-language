import React,{useState} from 'react';
import { AppBar,Toolbar,Typography,Paper,Grid,Button,TextField,Tooltip,Modal, Box } from '@mui/material';
import IconButton from '@mui/material/IconButton';
import SearchIcon from '@mui/icons-material/Search';
import RefreshIcon from '@mui/icons-material/Refresh';
import DataTable from '../../../components/user/DataTables';
import useAsycn from '../../../hooks/useAsync';
import axios from 'axios';
import CustomSnackbar from '../../../components/CustomSnackbar';
import PaymentOpDataTable from '../../../components/admin/paymentOpDataTable';
import ImageUpload from '../../../components/imageUpload';

export default function Content() {
  const columns = [
    { field: "id_payment", headerName: "No" },
    { field: "Name", headerName: "Payment Name"},
    { field: "Img",headerName: "image"},
    { field: "isActive",headerName: "isActive"},
  ];
  const [openModal, setOpenModal] = useState(false);
  const [name,setname] = useState(''); 
  const [img,setimg] = useState(''); 
  const [snackbarOpen, setSnackbarOpen] = useState(false);
  const [snackbarMessage, setSnackbarMessage] = useState("");
  const [snackbarSeverity, setSnackbarSeverity] = useState("success");
  const [editPaymentId, setEditPaymentId] = useState(null);
  const [originalName, setOriginalName] = useState('');
  const [originalImg, setOriginalImg] = useState('');
  const [imageURL, setImageURL] = useState(null); 

  const ApiUrl = process.env.REACT_APP_GET_ALL_PAYMENT_METHOD;
  const paymentlist = useAsycn(ApiUrl);
  const [searchText, setSearchText] = useState('');

  const filterData = Array.isArray(paymentlist)
  ? paymentlist.filter((row) => {
    return (
      row.name.toLowerCase().includes(searchText)
    );
    })
  : [];
  
  const rows = Array.isArray(filterData)
    ? filterData.map((row) => ({
        id_payment: row.id_payment,
        Name: row.name,
        Img: row.img,
        isActive: row.isActive ? "Active" : "Inactive",
      }))
    : [];

    const handleOpenModal = (data) => {
      if (data) {
        // Editing an existing payment method
        setEditPaymentId(data.id_payment);
        setname(data.Name);
        setOriginalName(data.Name);
        setOriginalImg(data.Img);
      } else {
        // Adding a new payment method
        setEditPaymentId(null);
        setname('');
        setOriginalName('');
        setOriginalImg('');
      }
      setOpenModal(true);
    };
  
    const handleCloseModal = () => {
      setOpenModal(false);
    };

  const handleSubmit = async () =>{
    const ApiPostPayment = process.env.REACT_APP_POST_PAYMENT_METHOD;
    const ApiEDITPayment = process.env.REACT_APP_EDIT_PAYMENT_METHOD;
    if (editPaymentId) {
      const dataEdit= {
        id_payment: editPaymentId,
        name: name,
        img: imageURL
      }
      // Editing an existing payment method
      await axios
        .put(`${ApiEDITPayment}`,dataEdit)
        .then((response) => {
          setSnackbarMessage("Payment method updated!");
          setTimeout(() => window.location.reload(), 3000);
        })
        .catch((error) => {
          console.log(error);
        });
    } else {
      // Adding a new payment method

      console.log([{ 
        name: name,
        img: imageURL
       }])
      await axios
        .post(ApiPostPayment, [{ 
          name: name,
          img: imageURL
         }])
        .then((response) => {
          setSnackbarMessage("Payment method added!");
          console.log(response);
          setTimeout(() => window.location.reload(), 3000);
        })
        .catch((error) => {
          console.log(error);
        });
    }
    setSnackbarOpen(true);
  }

  return (
    <Paper sx={{ maxWidth: 1500, margin: 'auto', overflow: 'scroll' }}>
      <AppBar
        position="static"
        color="default"
        elevation={0}
        sx={{ borderBottom: '1px solid rgba(0, 0, 0, 0.12)' }}
      >
        <Toolbar>
          <Grid container spacing={2} alignItems="center">
            <Grid item>
              <SearchIcon color="inherit" sx={{ display: 'block' }} />
            </Grid>
            <Grid item xs>
              <TextField
                fullWidth
                placeholder="Search by Payment Method, Payment Uid"
                InputProps={{
                  disableUnderline: true,
                  sx: { fontSize: 'default' },
                }}
                variant="standard"
                value={searchText}
                onChange={(e)=> setSearchText(e.target.value)}
              />
            </Grid>
            <Grid item>
              <Button variant="contained" sx={{ mr: 1 }} onClick={() => handleOpenModal(null)}>
                Add Payment
              </Button>
              <Tooltip title="Reload">
                <IconButton>
                  <RefreshIcon color="inherit" sx={{ display: 'block' }} />
                </IconButton>
              </Tooltip>
            </Grid>
          </Grid>
        </Toolbar>
      </AppBar>
      <PaymentOpDataTable columns={columns} rows={rows} handleOpenModal={handleOpenModal}/>

      <Modal open={openModal} onClose={handleCloseModal}>
        <Paper sx={{ padding: 2, minWidth: 400, margin: 'auto', mt: 4, position: "absolute", top: "50%", left: "50%", transform: "translate(-50%, -50%)", }}>
          <Typography variant="h6" gutterBottom>
          {editPaymentId ? "Edit Payment" : "Add Payment"}
          </Typography>
          <TextField
            fullWidth
            label="Payment Name"
            name="name"
            value={name}
            onChange={(e)=> setname(e.target.value)}
            sx={{ mb: 2 }}
          />
          <ImageUpload
            setImageURL={setImageURL}
          />
          <Box sx={{ display: 'flex', justifyContent: 'flex-end'  }}>
          <Button variant="contained" onClick={handleSubmit} sx={{ backgroundColor:'#226957' }}>
            {editPaymentId ? "Update Payment" : "Save Payment"}
          </Button>
          </Box>
        </Paper>
      </Modal>
      <CustomSnackbar
        open={snackbarOpen}
        setOpen={setSnackbarOpen}
        message={snackbarMessage}
        severity={snackbarSeverity}
      />
    </Paper>
  );
}