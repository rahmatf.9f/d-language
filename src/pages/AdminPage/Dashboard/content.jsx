import React,{useState} from 'react';
import { AppBar,Toolbar,Typography,Paper,Grid,Button,TextField,Tooltip,Modal,FormControl,InputLabel,Select,OutlinedInput,MenuItem,InputAdornment } from '@mui/material';
import IconButton from '@mui/material/IconButton';
import SearchIcon from '@mui/icons-material/Search';
import RefreshIcon from '@mui/icons-material/Refresh';
import useAsycn from '../../../hooks/useAsync';
import UserDataTable from '../../../components/admin/userDataTables';
import axios from 'axios';
import Visibility from '@mui/icons-material/Visibility';
import VisibilityOff from '@mui/icons-material/VisibilityOff';
import CustomSnackbar from '../../../components/CustomSnackbar';

export default function Content() {
  const columns = [
    { field: "User_Id", headerName: "No" },
    { field: "Name", headerName: "User Name"},
    { field: "Email",headerName: "Email"},
    { field: "Role",headerName: "Role"},
    { field: "IsActive",headerName: "Status Account"},
  ];
  const [openModal, setOpenModal] = useState(false);
  const [name,setname] = useState('');
  const [email,setemail] = useState('');
  const [password,setpassword] = useState('');
  const [role,setrole] = useState(['User','Admin']);
  const [snackbarOpen, setSnackbarOpen] = useState(false);
  const [snackbarMessage, setSnackbarMessage] = useState("");
  const [snackbarSeverity, setSnackbarSeverity] = useState("success");
  const [editUserId, seteditUserId] = useState(null);
  const [originalname, setOriginalname] = useState('');
  const [originalemail, setOriginalemail] = useState('');
  const [originalpassword, setOriginalpassword] = useState('');
  const [originalrolel, setOriginalrole] = useState('');
  const [showPassword, setShowPassword] = React.useState(false);


  const api = process.env.REACT_APP_GET_USER;
  const data = useAsycn(api);
  const [searchText, setSearchText] = useState('');

  const filterData = Array.isArray(data)
  ? data.filter((row) => {
    return (
      row.email.toLowerCase().includes(searchText) ||
      row.name.toLowerCase().includes(searchText)
    );
    })
  : [];
  const rowse = [
    {
      User_Id: '1',
      Name: 'User Name',
      Email: 'Email',
      Role: 'Role',
      IsActive: 'Active',
    },
    {
      User_Id: '1',
      Name: 'User Name',
      Email: 'Email',
      Role: 'Role',
      IsActive: 'Active',
    },
    {
      User_Id: '1',
      Name: 'User Name',
      Email: 'Email',
      Role: 'Role',
      IsActive: 'Active',
    },
    {
      User_Id: '1',
      Name: 'User Name',
      Email: 'Email',
      Role: 'Role',
      IsActive: 'Active',
    },
    {
      User_Id: '1',
      Name: 'User Name',
      Email: 'Email',
      Role: 'Role',
      IsActive: 'Active',
    },
    {
      User_Id: '1',
      Name: 'User Name',
      Email: 'Email',
      Role: 'Role',
      IsActive: 'Active',
    },
    {
      User_Id: '1',
      Name: 'User Name',
      Email: 'Email',
      Role: 'Role',
      IsActive: 'Active',
    },
  ]
  const rows = Array.isArray(filterData)
    ? filterData.map((row) => ({
        User_Id: row.user_Id,
        Name: row.name,
        Email: row.email,
        Role: row.role,
        IsActive: row.isActive ? "Active" : "Inactive",
      }))
    : [];

    const handleCloseModal = () => {
      setOpenModal(false);
    };
  
    const handleOpenModal = (data) => {
      if (data) {
        // Editing an existing payment method
        seteditUserId(data.User_Id);
        setname(data.Name);
        setemail(data.Email);
        setpassword(data.Password);
        setrole(data.Role);
        setOriginalname(data.Name);
        setOriginalemail(data.Email);
        setOriginalpassword('');
        setOriginalrole(data.Role);
      } else {
        // Adding a new payment method
        seteditUserId(null);
        setname('');
        setemail('');
        setpassword('');
        setrole('');
        setOriginalname('');
        setOriginalemail('');
        setOriginalpassword('');
        setOriginalrole('');
      }
      setOpenModal(true);
    };
    const handleSubmit = async () =>{
      const ApiPostUser = process.env.REACT_APP_POST_USER;
      const ApiEdittUser = process.env.REACT_APP_EDIT_USER;
      if (editUserId) {
        const dataEdit= {
          user_Id: editUserId,
          name: name,
          email: email,
          password: password,
          role: role,
          isActive: false
        }
        // Editing an existing payment method
        await axios
          .put(`${ApiEdittUser}`,dataEdit)
          .then((response) => {
            setSnackbarMessage("User updated!");
            setTimeout(() => window.location.reload(), 2500);
          })
          .catch((error) => {
            console.log(error);
          });
      } else {
        // Adding a new payment method
        const dataPost = {
          user_Id: 0,
          name: name,
          email: email,
          password: password,
          role: role,
          isActive: false
        }
        await axios
          .post(ApiPostUser, dataPost)
          .then((response) => {
            setSnackbarMessage("Payment method added!");
            console.log(response);
            setTimeout(() => window.location.reload(), 2500);
          })
          .catch((error) => {
            console.log(error);
          });

      }
      setSnackbarOpen(true);
    }

  return (
    <Paper sx={{ maxWidth: 1500, margin: 'auto' }}>
      <AppBar
        position="static"
        color="default"
        elevation={0}
        sx={{ borderBottom: '1px solid rgba(0, 0, 0, 0.12)' }}
      >
        <Toolbar>
          <Grid container spacing={2} alignItems="center">
            <Grid item>
              <SearchIcon color="inherit" sx={{ display: 'block' }} />
            </Grid>
            <Grid item xs>
              <TextField
                fullWidth
                placeholder="Search by Email addres, or User Name"
                InputProps={{
                  disableUnderline: true,
                  sx: { fontSize: 'default' },
                }}
                variant="standard"
                value={searchText}
                onChange={(e)=> setSearchText(e.target.value)}
              />
            </Grid>
            <Grid item>
            <Button variant="contained" sx={{ mr: 1 }} onClick={() => handleOpenModal(null)}>
                Add User
              </Button>
              <Tooltip title="Reload">
                <IconButton>
                  <RefreshIcon color="inherit" sx={{ display: 'block' }} />
                </IconButton>
              </Tooltip>
            </Grid>
          </Grid>
        </Toolbar>
      </AppBar>
      <UserDataTable columns={columns} rows={rows} handleOpenModal={handleOpenModal}/>
      <Modal open={openModal} onClose={handleCloseModal}>
        <Paper sx={{ padding: 2, maxWidth: 400, margin: 'auto', mt: 4, position: "absolute", top: "50%", left: "50%", transform: "translate(-50%, -50%)", }}>
          <Typography variant="h6" gutterBottom>
          {editUserId ? "Edit User" : "Add User"}
          </Typography>
          <TextField
            fullWidth
            label="User Name"
            name="name"
            value={name}
            onChange={(e)=> setname(e.target.value)}
            sx={{ mb: 2 }}
          />
          <TextField
            fullWidth
            label="email"
            name="email"
            value={email}
            onChange={(e)=> setemail(e.target.value)}
            sx={{ mb: 2 }}
          />
          {/* <TextField
            fullWidth
            label="password"
            name="password"
            type='password'
            value={password}
            onChange={(e)=> setpassword(e.target.value)}
            sx={{ mb: 2 }}
          /> */}
        <FormControl sx={{ mb: 2 }} variant="outlined" fullWidth>
          <InputLabel htmlFor="outlined-adornment-password">Password</InputLabel>
          <OutlinedInput
            id="outlined-adornment-password"
            type={showPassword ? 'text' : 'password'}
            endAdornment={
              <InputAdornment position="end">
                <IconButton
                  aria-label="toggle password visibility"
                  onClick={() => setShowPassword((show) => !show)}
                  onMouseDown={(event)=> event.preventDefault()}
                  edge="end"
                >
                  {showPassword ? <VisibilityOff /> : <Visibility />}
                </IconButton>
              </InputAdornment>
            }
            label="Password"
            name="password"
            onChange={(e)=> setpassword(e.target.value)}
          />
        </FormControl>

        <FormControl fullWidth sx={{ mb:2 }}>
        <InputLabel id="demo-simple-select-label">
          Roles
        </InputLabel>
        <Select
          labelId="demo-simple-select-label"
          id="demo-simple-select-label"
          value={role}
          label="Role"
          name='role'
          onChange={(e)=> setrole(e.target.value)}
        >
          <MenuItem value="User">User</MenuItem>
          <MenuItem value="Admin">Admin</MenuItem>
        </Select>
      </FormControl>
          <Button variant="contained" onClick={handleSubmit}>
            {editUserId ? "Update User" : "Save User"}
          </Button>
        </Paper>
      </Modal>
      <CustomSnackbar
        open={snackbarOpen}
        setOpen={setSnackbarOpen}
        message={snackbarMessage}
        severity={snackbarSeverity}
      />
    </Paper>
  );
}