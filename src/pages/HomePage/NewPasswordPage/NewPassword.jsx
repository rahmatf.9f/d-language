import React, { useState } from "react";
import { Box, Button, Grid, Paper, TextField, Typography } from "@mui/material";
import useMediaQuery from "@mui/material/useMediaQuery";
import { useLocation, useHistory } from "react-router-dom";

const NewPassword = () => {
  const isMobile = useMediaQuery("(max-width:600px)");
  const paperStyle = {
    display: "flex",
    flexDirection: "column",
    position: "absolute",
    width: isMobile ? "90%" : "616px",
    height: "450px",
    left: isMobile ? "5%" : "450px",
    top: "146px",
    boxShadow: "none",
  };
  const navigate = useHistory();
  const location = useLocation();
  const email = location.state?.email || "";

  const [password, setPassword] = useState("");
  const [confirmPassword, setConfirmPassword] = useState("");
  const handleSubmit = async () => {
    try {
      // Validate the data
      if (password !== confirmPassword) {
        console.log("Passwords do not match");
        return;
      }
      console.log(email)
      const response = await fetch(process.env.REACT_APP_NEW_PASSWORD, {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({ password, email }),
      });
        console.log(email+" dan"+ password);
      if (response.ok) {
        console.log("Password updated successfully");
        navigate.push("/login");
      } else {
        console.error("Error updating password");
      }
    } catch (error) {
      console.error("Error updating password:", error);
    }
  };

  return (
    <Box>
      <Grid>
        <Paper style={paperStyle}>
          <Typography
            style={{
              font: "Montserrat",
              fontSize: "24px",
              lineHeight: "29.26px",
              wight: "400",
            }}
          >
            Create Password
          </Typography>
          <form>
            <TextField
              label="New Password"
              type="password"
              fullWidth
              style={{
                marginTop: "60px",
                font: "Montserrat",
                fontSize: "14px",
                lineHeight: "17px",
              }}
              onChange={(e) => setPassword(e.target.value)}
            />
            <TextField
              label="Confirm New Password"
              type="password"
              fullWidth
              style={{
                marginTop: "24px",
                font: "Montserrat",
                fontSize: "14px",
                lineHeight: "17px",
              }}
              onChange={(e) => setConfirmPassword(e.target.value)}
            />
          </form>
          <div
            style={{
              display: "flex",
              justifyContent: "flex-end",
              marginTop: "40px",
            }}
          >
            <Button
              style={{
                background: "#EA9E1F",
                padding: "10px",
                width: "140px",
                height: "38",
                borderRadius: "8px",
                marginRight: "24px",
                color: "#FFFFFF",
              }}
              onClick={()=> navigate.push("/login")}
            >
              Cancel
            </Button>
            <Button
              style={{
                background: "#226957",
                padding: "10px",
                width: "140px",
                height: "38",
                borderRadius: "8px",
                color: "#FFFFFF",
              }}
              onClick={handleSubmit}
            >
              Submit
            </Button>
          </div>
        </Paper>
      </Grid>
    </Box>
  );
};

export default NewPassword;
