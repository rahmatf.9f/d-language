import { useState } from "react";
import { Grid, Paper, TextField, Typography, Button, Snackbar, Alert } from "@mui/material";
import useMediaQuery from "@mui/material/useMediaQuery";
import { useHistory } from "react-router-dom/cjs/react-router-dom";

const useForm = (initialValues) => {
  const [formValues, setFormValues] = useState(initialValues);

  const handleChange = (event) => {
    setFormValues({
      ...formValues,
      [event.target.name]: event.target.value,
    });
  };

  return [formValues, handleChange];
};

const Register = () => {
  const [formValues, handleChange] = useForm({
    name: "",
    email: "",
    password: "",
    confirmPassword: "",
  });
  const [error, setError] = useState(null);
  const isMobile = useMediaQuery("(max-width:900px)");
  const [openSnackbar, setOpenSnackbar] = useState(false);
  const [openErrorSnackbar, setOpenErrorSnackbar] = useState(false);
  const [errorSnackbarMessage, setErrorSnackbarMessage] = useState();

  const navigate = useHistory();
  const handleSubmit = async (event) => {
    event.preventDefault();
    if (formValues.password !== formValues.confirmPassword) {
      setError("Password and confirm password do not match");
      return;
    }
    setError(null);

    try {
      const response = await fetch(process.env.REACT_APP_REGISTER, {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          name: formValues.name,
          email: formValues.email,
          password: formValues.password,
          role : 'User'
        }),
      });

      if (response.ok) {
        alert("Please, check your email");
        setOpenSnackbar(true);
      } else {
        console.error("An error occurred:", await response.text());
        setErrorSnackbarMessage(await response.text());
        setOpenErrorSnackbar(true);
      }
    } catch (error) {
      console.error("An error occurred:", error);
      setErrorSnackbarMessage(error);
      setOpenErrorSnackbar(true);
    }
  };

  const paperStyle = {
    display: "flex",
    flexDirection: "column",
    position: "absolute",
    width: isMobile ? "90%" : "616px",
    height: "482px",
    left: isMobile ? "5%" : "450px",
    top: "146px",
    boxShadow: "none",
  };
  const welcomeStyle = {
    color: "#226957",
    fontSize: "24px",
    marginBottom: "16px",
  };
  const dLanguageStyle = {
    color: "#EA9E1F",
    fontSize: "36px",
  };
  const logInFirstStyle = {
    color: "#4F4F4F",
    fontSize: "16px",
    font: "Montserrat",
    marginBottom: "30px",
  };

  return (
      <div style={{ height: "100vh" }}>
        <Grid>
          <Paper style={paperStyle}>
            <Typography variant="h4" style={welcomeStyle}>
              Lets Join <span style={dLanguageStyle}>D'Language</span>
            </Typography>
            <Typography variant="caption" style={logInFirstStyle}>
              Please register first
            </Typography>
            {error && <p style={{ color: "red" }}>{error}</p>}
            <form onSubmit={handleSubmit}>
              <TextField
                fullWidth
                label="Name"
                name="name"
                onChange={handleChange}
                style={{ margin: "12px 0" }}
              />
              <TextField
                fullWidth
                label="Email"
                name="email"
                onChange={handleChange}
                style={{ margin: "12px 0" }}
              />
              <TextField
                fullWidth
                label="Password"
                type="password"
                name="password"
                onChange={handleChange}
                style={{ margin: "12px 0" }}
              />
              <TextField
                fullWidth
                label="Confirm Password"
                type="password"
                name="confirmPassword"
                onChange={handleChange}
                style={{ margin: "12px 0" }}
              />
              <div
                style={{
                  display: "flex",
                  justifyContent: "flex-end",
                  marginTop: "28px",
                }}
              >
                <Button
                  type="submit"
                  style={{
                    background: "#226957",
                    width: "140px",
                    height: "38px",
                    padding: "10px",
                    borderRadius: "8px",
                    color: "#ffffff",
                  }}
                >
                  Sign Up
                </Button>
              </div>
            </form>
            <div
              style={{
                display: "flex",
                justifyContent: "center",
                alignItems: "center",
                marginTop: "60px",
              }}
            >
              <Typography>
                Have an account{" "}
                <span
                  style={{ cursor: "pointer" }}
                  onClick={() => navigate.push("/login")}
                >
                  Login here
                </span>
              </Typography>
            </div>
          </Paper>
        </Grid>
        <Snackbar 
        open={openSnackbar}
        autoHideDuration={4000}
        onClose={() => setOpenSnackbar(false)}
        >
          <Alert
              onClose={() => setOpenSnackbar(false)}
              severity="success"
              sx={{ width: '100%' }}
          >
            please check your email
          </Alert>
        </Snackbar>
        <Snackbar
  open={openErrorSnackbar}
  autoHideDuration={4000}
  onClose={() => setOpenErrorSnackbar(false)}
>
<Alert
  onClose={() => setOpenErrorSnackbar(false)}
  severity="error"
  sx={{ width: '100%' }}
>
  An error occurred during registration:
  {errorSnackbarMessage && errorSnackbarMessage.toString()}
</Alert>
</Snackbar>
      </div>
  );
};

export default Register;
