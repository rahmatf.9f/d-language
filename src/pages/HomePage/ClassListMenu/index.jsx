import {
  Box,
  Typography,
  useTheme,
  useMediaQuery,
} from "@mui/material";
import React, { useState } from "react";
import HeaderListMenu from "../../../components/homepage/headerlistmenu";
import RecomenClass from "../../../components/homepage/CardRecomendClass";
import LoadingScreen from "../../../components/loading";
import useAsync from "../../../hooks/useAsync";
import { useParams } from "react-router-dom";

export default function ClassListMenu() {
  const theme = useTheme();
  const isMobile = useMediaQuery(theme.breakpoints.down("md"));
  const {name} = useParams();
  const [NavLink] = useState('detailclass');
  const detailApiMenu = `${process.env.REACT_APP_GET_LANGUAGE_CATEGORIES}?name=${name}`;
  const ApiUrl = `${process.env.REACT_APP_GET_LANGUAGE_COURSENAME}?name=${name}`;
  const datadetailMenu = useAsync(detailApiMenu);
  const dataRecomend = useAsync(ApiUrl);

  return (
    <Box
      component="main"
      sx={{
        marginX: isMobile ? 0 : "auto",
        marginTop: "auto",
        marginBottom: isMobile ? "5.6vh" : "10vh",
      }}
    >
      <Box>
        {datadetailMenu.length > 0 ? (
          datadetailMenu.map((data) => {
            return <HeaderListMenu items={data}/>
          })
        ) : (
          <LoadingScreen/>
        )}
      </Box>
      <Box sx={{ marginTop: "10vh", position: "relative" }}>
        <Typography
          sx={{ color: "#226957", fontWeight: 600, fontFamily: "Montserrat",textAlign:'center' }}
          variant="h5"
        >
          Class You Might Like
        </Typography>
        <Box
          sx={{
            display: "flex",
            flexDirection: isMobile ? "column" : "row",
            justifyContent: "center",
            alignItems: isMobile ?"center": 'center',
            textAlign: "center",
            marginTop: "40px",
          }}
        >
          {dataRecomend.length > 0 ? (
            dataRecomend.slice(0, 3).map((data) => {
              return <RecomenClass items={data} link={NavLink} />;
            })
          ) : (
            <LoadingScreen/>
          )}
        </Box>
      </Box>
    </Box>
  );
}
