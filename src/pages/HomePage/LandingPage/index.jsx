import React from 'react';
import HeaderPage from '../../../components/homepage/HeaderPage';
import RecomendedClass from '../../../components/RecomendedClass';
import Benefit from '../../../components/Benefit';
import AvailabelLanguage from '../../../components/AvailableLanguage';

const LandingPage = () => {
  return (
    <div>
      <HeaderPage />
      <RecomendedClass/>
      <Benefit />
      <AvailabelLanguage />
    </div>
  );
};

export default LandingPage;
