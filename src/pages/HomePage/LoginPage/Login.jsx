import { useState } from "react";
import { Grid, Paper, TextField, Typography, Button, Box } from "@mui/material";
import useMediaQuery from "@mui/material/useMediaQuery";
import { useHistory } from "react-router-dom";
const Login = () => {
  const navigate = useHistory();
  const isMobile = useMediaQuery("(max-width:900px)");
  const paperStyle = {
    display: "flex",
    flexDirection: "column",
    position: "absolute",
    justifyContent:"center",
    width: isMobile ? "90%" : "616px",
    height: "450px",
    left: isMobile ? "5%" : "450px",
    top: "146px",
    boxShadow: "none",
  };
  const welcomeStyle = {
    color: "#226957",
    fontSize: "24px",
    font: "Montserrat",
    marginBottom: "16px",
  };
  const logInFirstStyle = {
    color: "#4F4F4F",
    fontSize: "16px",
    font: "Montserrat",
    marginBottom: "30px",
  };

  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  const handleLogin = () => {
    // Kirim permintaan login ke backend
    fetch(process.env.REACT_APP_LOGIN, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${localStorage.getItem("token")}`
      },
      body: JSON.stringify({ email, password }),
    })
      .then((response) => response.json())
      .then((data) => {
        if ('role' in data) {
          localStorage.setItem("loggedIn", true);
          localStorage.setItem("userRole", data.role);
          localStorage.setItem("user_id", data.userid);
          window.location.href = data.role === 'User' ? '/' : '/admin';
        } else {
          alert('User is not active or invalid email or password');
        }
      })
      .catch((error) => {
        console.error(error);
        alert(error);
      });
  };  

  return (
    <Box >
      <Grid>
        <Paper style={paperStyle}>
          <Typography variant="h4" style={welcomeStyle}>
            Welcome Back!
          </Typography>
          <Typography variant="caption" style={logInFirstStyle}>
            Please login first
          </Typography>
          <form>
            <TextField
              fullWidth
              label="Email"
              style={{ margin: "12px 0" }}
              value={email}
              onChange={(e) => setEmail(e.target.value)}
            />
            <TextField
              fullWidth
              label="Password"
              type="password"
              style={{ margin: "12px 0" }}
              value={password}
              onChange={(e) => setPassword(e.target.value)}
            />
          </form>
          <Typography
            style={{ marginTop: "12px", fontSize: "16px", font: "Montserrat" }}
          >
            Forgot Password?
            <span
              onClick={() => navigate.push("/reset-password")}
              style={{ cursor: "pointer" }}
            >
              Click Here
            </span>
          </Typography>
          <div
            style={{
              display: "flex",
              justifyContent: "flex-end",
              marginTop: "40px",
            }}
          >
            <Button
              style={{
                background: "#226957",
                width: "140px",
                height: "38px",
                padding: "10px",
                borderRadius: "8px",
                color: "#ffffff",
              }}
              onClick={handleLogin}
            >
              Login
            </Button>
          </div>
          <div
            style={{
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
              marginTop: "60px",
            }}
          >
            <Typography>
              Don't have an account?
              <span
                onClick={() => navigate.push("/register")}
                style={{ cursor: "pointer" }}
              >
                Sign Up Here
              </span>
            </Typography>
          </div>
        </Paper>
      </Grid>
    </Box>
  );
};

export default Login;
