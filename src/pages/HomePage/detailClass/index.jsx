import { Box,Typography,useMediaQuery,useTheme, } from "@mui/material";
import React, { useState } from "react";
import AddClass from "../../../components/homepage/addClass";
import Classdescription from "../../../components/homepage/addClass/Classdescription";
import RecomenClass from "../../../components/homepage/CardRecomendClass";
import LoadingScreen from "../../../components/loading"
import useAsync from "../../../hooks/useAsync";
import { useParams } from "react-router-dom";

export default function DetailClass() {
  const [NavLink] = useState('detailclass');
  const Theme = useTheme();
  const isMobile = useMediaQuery(Theme.breakpoints.down('md'));

  const {id} = useParams();
  
  const ApiUrl = process.env.REACT_APP_GET_ALL_COURSE;
  const detailAPIUrl =
  `${process.env.REACT_APP_GET_ALL_COURSE}?id=${id}`;
  
  const dataClass = useAsync(ApiUrl);
  const dataDetailClass = useAsync(detailAPIUrl);

  return (
    <Box component="main">
      <main>
        <Box sx={{ paddingBottom: 5, paddingTop: 13 }}>
          {dataDetailClass.length > 0 ? (
            // eslint-disable-next-line array-callback-return
            dataDetailClass.map((items) => {
                return (
                  <Box>
                    <AddClass items={items} key={items.id_class} />
                    <Classdescription items={items} key={items.id_class}/>
                  </Box>
                );
            })
          ) : (
            <LoadingScreen/>
          )}
      <Box sx={{ marginTop: "10vh"}}>
        <Typography
          sx={{ color: "#226957", fontWeight: 600, fontFamily: "Montserrat",textAlign:'center' }}
          variant="h5"
        >
          Class You Might Like
        </Typography>
        <Box
          sx={{
            display: "flex",
            flexDirection: isMobile ? "column" : "row",
            justifyContent: "center",
            alignItems: isMobile ?"center": 'center',
            textAlign: "center",
            marginTop: "40px",
          }}
        >
          {dataClass.length > 0 ? (
            dataClass.slice(0, 3).map((data) => {
              return <RecomenClass items={data} link={NavLink} key={data.id_class} />;
            })
          ) : (
            <LoadingScreen/>
          )}
        </Box>
      </Box>
        </Box>
      </main>
    </Box>
  );
}
