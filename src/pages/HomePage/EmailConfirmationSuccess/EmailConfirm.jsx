import React from "react";
import { Typography, Button } from "@mui/material";
import Navbar from "../../../components/NavbarEmailConfirm";
import { useHistory} from 'react-router-dom'
const EmailConfirm = () => {
  const navigate = useHistory();
  return (
    <>
      <Navbar />
      <div
        style={{
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
          flexDirection: "column",
        }}
      >
        <img src="/Images/transaction/succesTransaction.png"alt="." style={{ marginTop: "54px" }} />
        <Typography
          style={{ marginTop: "44px", color: "#226957", fontSize: "24px" }}
        >
          Email Confirmation Success
        </Typography>
        <Typography
          style={{ marginTop: "8px", fontSize: "16px", color: "#4F4F4F" }}
        >
          Thanks for confirmation your email, please login first
        </Typography>
        <Button
          onClick={() => navigate.push("/login")}
          style={{
            marginTop: "40px",
            background: "#226957",
            width: "140px",
            height: "38px",
            padding: "16px 24px",
            borderRadius: "6px",
            color: "#ffffff",
          }}
        >
          Login Here
        </Button>
      </div>
    </>
  );
};

export default EmailConfirm;
