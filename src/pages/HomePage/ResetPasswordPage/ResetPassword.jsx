import React, { useState } from "react";
import { Button, Grid, Paper, TextField, Typography } from "@mui/material";
import useMediaQuery from "@mui/material/useMediaQuery";
import { useHistory } from "react-router-dom";

const ResetPassword = () => {
  const [email, setEmail] = useState("");
  const navigate = useHistory();
  const isMobile = useMediaQuery("(max-width:600px)");
  const paperStyle = {
    display: "flex",
    flexDirection: "column",
    position: "absolute",
    width: isMobile ? "90%" : "616px",
    height: "450px",
    left: isMobile ? "5%" : "332px",
    top: "146px",
    boxShadow: "none",
  };

  const handleResetPassword = async () => {
    // Call the back-end API to reset the password
    const response = await fetch(process.env.REACT_APP_RESET_PASSWORD, {
      method: "POST",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify({ email })
    });
    if (response.ok) {
      navigate.push("/new-password", { email });
    } else {
        alert('User not found');
    }
  };

  return (
    <div>
      <Grid>
        <Paper style={paperStyle}>
          <Typography
            style={{
              font: "Montserrat",
              fontSize: "24px",
              lineHeight: "29.26px",
              wight: "400",
            }}
          >
            Reset Password
          </Typography>
          <Typography
            style={{
              font: "Montserrat",
              fontSize: "16px",
              lineHeight: "19.5px",
              wight: "400",
              color: "#4F4F4F",
              marginTop: "16px",
            }}
          >
            Please enter your email address
          </Typography>
          <form>
            <TextField
              label="Email"
              fullWidth
              style={{
                marginTop: "60px",
                font: "Montserrat",
                fontSize: "14px",
                lineHeight: "17px",
              }}
              onChange={e => setEmail(e.target.value)}
            />
          </form>
          <div
            style={{
              display: "flex",
              justifyContent: "flex-end",
              marginTop: "40px",
            }}
          >
            <Button
              style={{
                background: "#EA9E1F",
                padding: "10px",
                width: "140px",
                height: "38",
                borderRadius: "8px",
                marginRight: "24px",
                color: "#FFFFFF",
              }}
            >
              Cancel
            </Button>
            <Button
              style={{
                background: "#226957",
                padding: "10px",
                width: "140px",
                height: "38",
                borderRadius: "8px",
                color: "#FFFFFF",
              }}
              onClick={handleResetPassword}
            >
              Confirm
            </Button>
          </div>
        </Paper>
      </Grid>
    </div>
  );
};

export default ResetPassword;
