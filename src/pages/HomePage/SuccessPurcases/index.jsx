import React from "react";
import Navbar from "../../../components/NavbarEmailConfirm";
import SuccessPurcase from "../../../components/SuccessPurcase";
const SuccessPurcases = () => {

  return (
    <>
      <Navbar />
      <div
        style={{
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
          flexDirection: "column",
        }}
      >
        <SuccessPurcase/>
      </div>
    </>
  );
};

export default SuccessPurcases;
