// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getStorage } from 'firebase/storage';
const firebaseConfig = {
  apiKey: "AIzaSyAcS5v0tFSsuFhiAp60dkPey7EyhQNUW_I",
  authDomain: "dlanguages-8232a.firebaseapp.com",
  projectId: "dlanguages-8232a",
  storageBucket: "dlanguages-8232a.appspot.com",
  messagingSenderId: "495318914503",
  appId: "1:495318914503:web:ebebbc8a253cf08decb5e2"

};


// Initialize Firebase
const app = initializeApp(firebaseConfig);
export const storage = getStorage(app);