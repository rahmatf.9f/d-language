import { createContext } from "react";
import PeopleIcon from '@mui/icons-material/People';
import DnsRoundedIcon from '@mui/icons-material/DnsRounded';
import PublicIcon from '@mui/icons-material/Public';
import SettingsEthernetIcon from '@mui/icons-material/SettingsEthernet';
import SettingsInputComponentIcon from '@mui/icons-material/SettingsInputComponent';
import PaymentsIcon from '@mui/icons-material/Payments';
const categories = [
    {
      id: 'Build',
      children: [
        {
          id: 'User Pages',
          icon: <PeopleIcon />,
          active: true,
        },
        { id: 'Payment Menu', icon: <PaymentsIcon /> },
        { id: 'Invoice', icon: <DnsRoundedIcon /> },
      ],
    }
  ];

const CategoryList = createContext(categories);

export default CategoryList;