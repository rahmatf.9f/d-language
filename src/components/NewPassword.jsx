import { Button, Grid, Paper, TextField, Typography } from "@mui/material"

const NewPassword = () => {
    const paperStyle = {
        display: 'flex',
        flexDirection: 'column',
        position: 'absolute',
        width: '616px',
        height: '450px',
        left: '332px',
        top: '146px',
        boxShadow: 'none'
    };
  return (
    <div>
        <Grid>
            <Paper  style={paperStyle}>
                <Typography style={{ font:'Montserrat', fontSize:'24px', lineHeight:'29.26px', wight:'400'}}>Create Password</Typography>
                <form>
                    <TextField label='New Password' type="password" fullWidth style={{marginTop:'60px', font:'Montserrat', fontSize:'14px', lineHeight:'17px'}}/>
                    <TextField label='Confirm New Password' type="password" fullWidth style={{marginTop:'24px', font:'Montserrat', fontSize:'14px', lineHeight:'17px'}}/>
                </form>
                <div style={{ display: 'flex', justifyContent: 'flex-end', marginTop:'40px' }}>
                    <Button style={{ background:'#EA9E1F', padding:'10px', width:'140px', height:'38', borderRadius:'8px', marginRight:'24px', color:'#FFFFFF'}}>Cancel</Button>
                    <Button style={{ background:'#226957', padding:'10px', width:'140px', height:'38', borderRadius:'8px', color:'#FFFFFF'}}>Submit</Button>
                </div>
            </Paper>
        </Grid>
    </div>
  )
}

export default NewPassword
