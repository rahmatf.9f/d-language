import React from "react";
import {
  Box,
  Container,
  Typography,
  CardActionArea,
  CardActions,
  Button,
  Card,
  CardContent,
  CardMedia,
  useTheme,
  useMediaQuery,
} from "@mui/material";
import { useHistory } from "react-router-dom";
export default function RecomenClass({items,link}) {
  const history = useHistory();
  const handleButtonClick = () => {
      const itemId = items.id_class; // Assuming item.id contains the ID of the item
      history.push(`/${link}/${itemId}`);
      setTimeout(() => window.location.reload(), 100);
    };

  const theme = useTheme();
  const isMobile = useMediaQuery(theme.breakpoints.down("md"));

  return (
    <Card sx={{ minWidth: 350, maxWidth: 350,minHeight:'370px',maxHeight:'370px', marginX:isMobile ? '0' : '10px',marginY:isMobile ? "10px" : "0" }}>
      <CardActionArea onClick={handleButtonClick} >
        <CardMedia
          component="img"
          height="140"
          image={'/' + items.img}
          alt={items.name}
        />
        <CardContent sx={{ textAlign: "left" }}>
          <Box sx={{ marginBottom: "40px" }}>
            <Typography gutterBottom variant="body1" component="div" color="text.secondary">
              {items.name}
            </Typography>
            <Typography variant="h6" color="text.primary">
              {items.description}
            </Typography>
          </Box>
          <Typography variant="body1" color="#226957" sx={{ fontWeight:500 }}>
            IDR {items.price}
          </Typography>
        </CardContent>
      </CardActionArea>
    </Card>
  );
}
