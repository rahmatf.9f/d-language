import React from 'react';
import { Box, Container, Typography } from '@mui/material';

export default function HeaderListMenu({items}) {

    return(
        <Box>
        <Box sx={{position: 'relative'}}>
            <img src="/Images/headerList.png" alt="header" style={{ width: '100%',height: 'auto',maxWidth: '100%' }} />
        </Box>
        <Box
            sx={{
                display: 'flex',
                flexDirection: 'row',
                justifyContent: 'Left',
                textAlign:'Left',
                marginTop:'40px'
            }}
            >
            <Container>
                <Box sx={{ marginY:'15px' }}>
                    <Typography variant='h5' style={{ fontFamily:"Montserrat" }}>
                        {items.name}
                    </Typography>
                </Box>
                <Box sx={{ textAlign:'justify' }}>
                    <Typography sx={{ fontFamily:"Montserrat",fontWeight:300,fontSize:'14px' }}>
                        {items.description}
                    </Typography>
                </Box>
            </Container>
        </Box>
    </Box>
    )
}