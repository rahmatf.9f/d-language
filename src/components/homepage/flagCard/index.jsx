import React from 'react'
import { Card, CardHeader, CardMedia, CardContent, Typography,CardActionArea,Box,useMediaQuery} from '@mui/material';
import { useHistory } from "react-router-dom";

export default function FlagCards({ item,link }) {
    const isMobile = useMediaQuery("(max-width:600px)");
    const history = useHistory();
    const handleButtonClick = () => {
      const itemName = item.name;
      history.push(`/${link}/${itemName}`);
    };
    return(
        <Card
              sx={{
                width: isMobile ? "150px" : "232px", 
                height: isMobile ? "160px" : "218",
              }}
            >
              <CardActionArea onClick={handleButtonClick}>
              <CardMedia
                component="img"
                alt="Card Image"
                image={item.img}
                sx={{
                  height: isMobile ? "100px" : "133.33px", 
                  width: isMobile ? "150px" : "200px", 
                  borderRadius: "8px",
                  margin: "auto",
                  padding: "2px",
                  marginTop: isMobile ? "5px" : "15px", 
                }}
              />
              <CardContent sx={{ textAlign: "center" }}>
                {item.name}
              </CardContent>
              </CardActionArea>
            </Card>
    )
}