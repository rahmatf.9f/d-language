import React,{useState} from "react";
import { useHistory } from "react-router-dom";
import {
  AppBar,
  Toolbar,
  Typography,
  IconButton,
  Box,
  useTheme,
  useMediaQuery,
  Button,
  Drawer,
  List,
  ListItem,
  ListItemIcon,
  ListItemText,
  ListItemButton,
} from "@mui/material";
import MenuIcon from "@mui/icons-material/Menu";
import LoginIcon from "@mui/icons-material/Login";
import PersonAddIcon from "@mui/icons-material/PersonAdd";

const Navbar = () => {

  const [DrawerOpen, setDrawerOpen] = useState(false);
  const handleDrawerToggle = () => {
    setDrawerOpen(!DrawerOpen);
  };
  const appBar = {
    backgroundColor: "#ffffff",
  };
  const theme = useTheme();
  const isMobile = useMediaQuery(theme.breakpoints.down("md"));
  const history = useHistory();
  const drawerItems = [
    {
      label: "LOGIN",
      icon: <LoginIcon />,
      path: "/login",
    },
    {
      label: "SIGN UP",
      icon: <PersonAddIcon />,
      path: "/register",
    },
  ];
  const logIn = {
    color: "inherit",
    backgroundColor: "#226957",
    borderRadius: "8px",
    marginRight: "20px",
    // padding: "10px 20px 10px 20px",
    height: "40px",
    width: "100px",
  };
  const signUp = {
    color: "inherit",
    backgroundColor: "#EA9E1F",
    borderRadius: "8px",
    // padding: "10px 20px 10px 20px",
    height: "40px",
    width: "100px",
  };

  const renderDrawerItems = () => {
    return (
      <List>
        {drawerItems.map((item, index) => (
          <ListItem key={index}>
            <ListItemButton onClick={() => history.push(`${item.path}`)}>
              <ListItemIcon>{item.icon}</ListItemIcon>
              <ListItemText primary={item.label} />
            </ListItemButton>
          </ListItem>
        ))}
      </List>
    );
  };

  return (
    <Box>
      <AppBar position="relative" style={appBar}>
        <Toolbar
          sx={{
            marginX: isMobile ? 0 : "100px",
            marginY: isMobile ? "0.6vh" : "auto",
          }}
        >
          <img src="/Images/logo.png" alt="logo" />
          <Typography
            variant="h5"
            noWrap
            component="a"
            href="/"
            sx={{
              color: "text.primary",
              textDecoration: "none",
              fontFamily: "Montserrat",
              marginLeft: "10px",
            }}
          >
            Language
          </Typography>

          <Box sx={{ flexGrow: 1 }} />

          {isMobile ? (
            <IconButton onClick={handleDrawerToggle}>
              <MenuIcon />
            </IconButton>
          ) : (
            <Box
              sx={{ display: { xs: "none", md: "flex" }, alignItems: "center" }}
            >
              <Button onClick={() => history.push("/login")} style={logIn}>
                                <Typography
                  style={{
                    fontFamily: "Montserrat",
                    fontSize: "16px",
                    color: "white",
                    fontWeight: 500,
                  }}
                >
                  Login
                </Typography>
              </Button>
              <Button onClick={() => history.push("/register")} style={signUp}>
                                <Typography
                  style={{
                    fontFamily: "Montserrat",
                    fontSize: "16px",
                    color: "white",
                    fontWeight: 500,
                  }}
                >
                  Sign Up
                </Typography>
              </Button>
            </Box>
          )}
        </Toolbar>
      </AppBar>
      <Drawer anchor="right" open={DrawerOpen} onClose={handleDrawerToggle}>
        {renderDrawerItems()}
      </Drawer>
    </Box>
  );
};

export default Navbar;