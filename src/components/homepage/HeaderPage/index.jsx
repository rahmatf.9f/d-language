import React from "react";
import { Box, Typography, useMediaQuery } from "@mui/material";

const HeaderPage = () => {
  const isMobile = useMediaQuery("(max-width:600px)");
  return (
    <div>
      {isMobile ? (
        <>
          <Box
            sx={{
              marginTop: 0,
              position: "relative",
            }}
          >
            <img
              src="/Images/headerImage.png"
              alt="header"
              style={{
                width: "100%",
                height: "150px",
              }}
            />
            <Box
              sx={{
                position: "absolute",
                top: 0,
                left: 0,
                right: 0,
                bottom: 0,
                display: "flex",
                flexDirection: "column",
                justifyContent: "center",
                alignItems: "center",
              }}
            >
              <Typography
                style={{
                  color: "#FFFFFF",
                  fontSize: "18px",
                }}
              >
                Learn different languagaes to hone
              </Typography>
              <Typography
                style={{
                  color: "#FFFFFF",
                  fontSize: "18px",
                }}
              >
                your communication skills
              </Typography>
              <br />
              <Typography style={{ color: "#FFFFFF", fontSize: "10px" }}>
                All the languages you are looking for are available here, sho
                what are you waiting for
              </Typography>
              <Typography
                style={{
                  color: "#FFFFFF",
                  fontSize: "10px",
                }}
              >
                and immediately improve your languages skills
              </Typography>
            </Box>
          </Box>
          <Box
            sx={{
              display: "flex",
              flexDirection: "column",
              justifyContent: "center",
              alignItems: "center",
              textAlign: "center",
              marginTop: "40px",
            }}
          >
            <Box sx={{ margin: "0 10px", width: "300px" }}>
              <Typography
                sx={{
                  color: "#226957",
                  fontWeight: "700",
                  fontSize: "35px",
                  marginBottom: "20px",
                }}
              >
                100+
              </Typography>
              <Typography
                sx={{
                  fontSize: "18px",
                }}
              >
                Chose the class you like and get the skills
              </Typography>
            </Box>
            <Box
              sx={{
                margin: "0 10px",
                width: "300px",
              }}
            >
              <Typography
                sx={{
                  color: "#226957",
                  fontWeight: "700",
                  fontSize: "35px",
                  marginBottom: "20px",
                  marginTop: "15px",
                }}
              >
                50+
              </Typography>
              <Typography
                sx={{
                  fontSize: "18px",
                }}
              >
                Having teachers who are highly skilled and competent in the
                language
              </Typography>
            </Box>
            <Box
              sx={{
                margin: "0 10px",
                width: "300px",
              }}
            >
              <Typography
                sx={{
                  color: "#226957",
                  fontWeight: "700",
                  fontSize: "35px",
                  marginBottom: "20px",
                  marginTop: "15px",
                }}
              >
                10+
              </Typography>
              <Typography
                sx={{
                  fontSize: "18px",
                }}
              >
                Many alumni become ministry employees because of their excelent
                language skills
              </Typography>
            </Box>
          </Box>
        </>
      ) : (
        <>
          <Box sx={{ marginTop:0, position: "relative" }}>
            <img src="/Images/headerImage.png" alt="header" style={{ width: "100%" }} />
            <Box
              sx={{
                position: "absolute",
                top: 0,
                left: 0,
                right: 0,
                bottom: 0,
                display: "flex",
                flexDirection: "column",
                justifyContent: "center",
                alignItems: "center",
              }}
            >
              <Typography
                style={{
                  color: "#FFFFFF",
                  fontSize: "30px",
                }}
              >
                Learn different languagaes to hone
              </Typography>
              <Typography
                style={{
                  color: "#FFFFFF",
                  fontSize: "30px",
                }}
              >
                your communication skills
              </Typography>
              <br />
              <Typography
                style={{
                  color: "#FFFFFF",
                  fontSize: "20px",
                }}
              >
                All the languages you are looking for are available here, sho
                what are you waiting for
              </Typography>
              <Typography
                style={{
                  color: "#FFFFFF",
                  fontSize: "20px",
                }}
              >
                and immediately improve your languages skills
              </Typography>
            </Box>
          </Box>
          <Box
            sx={{
              display: "flex",
              flexDirection: "row",
              justifyContent: "center",
              textAlign: "center",
              marginTop: "40px",
            }}
          >
            <Box sx={{ margin: "0 10px", width: "300px" }}>
              <Typography
                sx={{
                  color: "#226957",
                  fontWeight: "700",
                  fontSize: "35px",
                  marginBottom: "25px",
                }}
              >
                100+
              </Typography>
              <Typography
                sx={{
                  fontSize: "18px",
                }}
              >
                Chose the class you like and get the skills
              </Typography>
            </Box>
            <Box
              sx={{
                margin: "0 10px",
                width: "300px",
              }}
            >
              <Typography
                sx={{
                  color: "#226957",
                  fontWeight: "700",
                  fontSize: "35px",
                  marginBottom: "25px",
                }}
              >
                50+
              </Typography>
              <Typography
                sx={{
                  fontSize: "18px",
                }}
              >
                Having teachers who are highly skilled and competent in the
                language
              </Typography>
            </Box>
            <Box sx={{ margin: "0 10px", width: "300px" }}>
              <Typography
                sx={{
                  color: "#226957",
                  fontWeight: "700",
                  fontSize: "35px",
                  marginBottom: "25px",
                }}
              >
                10+
              </Typography>
              <Typography
                sx={{
                  fontSize: "18px",
                }}
              >
                Many alumni become ministry employees because of their excelent
                language skills
              </Typography>
            </Box>
          </Box>
        </>
      )}
    </div>
  );
};

export default HeaderPage;
