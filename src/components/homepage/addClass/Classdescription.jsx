import React from "react";
import { Box, Container, Typography } from "@mui/material";

export default function Classdescription({ items }) {
  return (
    <Box
      sx={{
        display: "flex",
        flexDirection: "row",
        justifyContent: "Left",
        textAlign: "Left",
        marginTop: "40px",
      }}
    >
      <Container>
        <Box sx={{ marginY: "15px" }}>
          <Typography
            variant="h5"
            style={{ fontFamily: "Montserrat", fontWeight: 600 }}
          >
            Description
          </Typography>
        </Box>
        <Box sx={{ textAlign: "justify" }}>
          <Typography paragraph={true}
            sx={{ fontFamily: "Montserrat", fontWeight: 300, fontSize: "14px" }}
          >
            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
            eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim
            ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut
            aliquip ex ea commodo consequat. Duis aute irure dolor in
            reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla
            pariatur. Excepteur sint occaecat cupidatat non proident, sunt in
            culpa qui officia deserunt mollit anim id est laborum.
            
          </Typography>
          <Typography paragraph={true}
            sx={{ fontFamily: "Montserrat", fontWeight: 300, fontSize: "14px" }}
          >
            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
            eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim
            ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut
            aliquip ex ea commodo consequat. Duis aute irure dolor in
            reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla
            pariatur. Excepteur sint occaecat cupidatat non proident, sunt in
            culpa qui officia deserunt mollit anim id est laborum.
            
          </Typography>
        </Box>

      </Container>
    </Box>
  );
}
