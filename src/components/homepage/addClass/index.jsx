import React, { useContext, useState } from "react";
import {
  Box,
  Container,
  Typography,
  useTheme,
  useMediaQuery,
  Grid,
  InputLabel,
  MenuItem,
  FormControl,
  Select,
  Button,
} from "@mui/material";
import useFormatPrice from "../../../hooks/useFormatPrice";
import { useHistory } from "react-router-dom";
import {UserLogin} from "../../../context/UserLogin";
import CustomSnackbar from "../../CustomSnackbar";

export default function AddClass({ items }) {
  const {loggedIn,user} = useContext(UserLogin);
  const [snackbarOpen, setSnackbarOpen] = useState(false);
  const [snackbarMessage, setSnackbarMessage] = useState("");
  const [snackbarSeverity, setSnackbarSeverity] = useState("success");

  const [schedule, setSchedule] = useState("");
  // const [cart, setCart] = useState([]);
  const history = useHistory();

  const formatDate = (dateString) => {
    const date = new Date(dateString);
    const options = { weekday: "long", day: "numeric", month: "long", year: "numeric" };
    return date.toLocaleDateString("en-US", options);
  };

  const handleChange = (event) => {
    setSchedule(event.target.value);
  };

  const apiPost = process.env.REACT_APP_POST_CART;

  const handleCart = () => {
    if(loggedIn){
      const selectSchedule = items.schedules.find((option) => option.schedule_detail === schedule);
      if(selectSchedule){
        const newItemsCart = {
          fk_id_class: items.id_class,
          fk_id_schedule: selectSchedule.id_schedule,
          fk_id_user: user,
          status: "nopaid"
        }
        // setCart([...cart, newItemsCart])
        fetch(apiPost,{
          method: "POST",
          headers:{
            "Content-Type": "application/json"
          },
          body: JSON.stringify(newItemsCart)
        })
        .then((response) => {
          if (response.ok) {
            setSnackbarOpen(true);
            setSnackbarMessage("Item added to cart!");
            setSnackbarSeverity("success");
          } else{
            throw new Error("Failed to Add Item to Cart");
          }
        })
        .catch((error)=>{
          console.error(error);
          setSnackbarOpen(true);
          setSnackbarMessage("Failed to Add Item to Cart");
          setSnackbarSeverity("error");
        })
      }
    } else {
      history.push("/login");
    }
  }

  const handleBuynow = () => {
    if(loggedIn){
      const selectSchedule = items.schedules.find((option) => option.schedule_detail === schedule);
      if(selectSchedule){
        const newItemBuy = {
          fk_id_class: items.id_class,
          fk_id_schedule: selectSchedule.id_schedule,
          fk_id_user: user,
          status: "nopaid"
        }
        fetch(apiPost,{
          method: "POST",
          headers:{
            "Content-Type": "application/json"
          },
          body: JSON.stringify(newItemBuy)
        })
        .then((response) => {
          if (response.ok) {
            history.push("/checkout");
          } else{
            throw new Error("Failed to Add Item to Cart");
          }
        })
        .catch((error)=>{
          console.error(error);
          setSnackbarOpen(true);
          setSnackbarMessage("Failed to Add Item to Cart");
          setSnackbarSeverity("error");
        })
    } else {
      history.push("/");
      }
    }
  }

  const theme = useTheme();
  const isMobile = useMediaQuery(theme.breakpoints.down("md"));
  return (
    <Container>
      <Grid
        spacing={2}
        sx={{
          display: "flex",
          flexDirection: isMobile ? "column" : "row",
          textAlign: "left",
        }}
      >
        <Grid item xs={4} md={8} lg={12} sx={{ marginBottom: isMobile ? '10px': 0 }}>
          <img
            src={'/' + items.img}
            height= {isMobile ? '100%' : '250px'}
            width="100%"
            alt={items.name}
          />
        </Grid>
        <Grid
          item
          xs={4}
          sx={{
            display: "flex",
            flexDirection: "column",
            justifyContent: "space-between",
            marginLeft: isMobile ? 0 : '30px',
          }}
        >
          <Box>
            <Box>
              <Typography
                variant="subtitle2"
                color="text.secondary"
                sx={{
                  marginBottom: "8px",
                  fontSize: "16px",
                  fontFamily: "Montserrat",
                  fontWeight: 400,
                }}
              >
                {items.name}
              </Typography>
              <Typography
                variant="h5"
                color="text.primary"
                sx={{
                  marginBottom: "8px",
                  fontSize: "24px",
                  fontFamily: "Montserrat",
                  fontWeight: 600,
                }}
              >
                {items.description}
              </Typography>
              <Typography
                variant="h6"
                sx={{
                  color: "#EA9E1F",
                  marginBottom: "8px",
                  fontSize: "24px",
                  fontFamily: "Montserrat",
                  fontWeight: 600,
                }}
              >
                IDR {useFormatPrice(items.price)}
              </Typography>
            </Box>

            <FormControl sx={{ minWidth: 300,marginBottom: isMobile ? '10px': 0 }}>
              <InputLabel id="demo-simple-select-label">
                Select Schedule
              </InputLabel>
              <Select
                labelId="demo-simple-select-label"
                id="demo-simple-select"
                value={schedule}
                label="Select Schedule"
                onChange={handleChange}
              >
                <MenuItem value="">
                  <em>None</em>
                </MenuItem>
                {items.schedules.map(option=> (
                  <MenuItem value={option.schedule_detail}>{formatDate(option.schedule_detail)}</MenuItem>
                ))}
              </Select>
            </FormControl>
          </Box>

          <Box sx={{ display: "flex", justifyContent: isMobile ? 'center':'flex-start' }}>
            <Button
              variant="contained"
              sx={{
                marginRight: "10px",
                marginBottom: "10px",
                backgroundColor: "#EA9E1F",
                minWidth: isMobile ? "150px" : "200px",
              }}
              onClick={handleCart}
            >
              Add to Cart
            </Button>
            <Button
              type="submit"
              variant="contained"
              sx={{
                marginRight: "10px",
                marginBottom: "10px",
                backgroundColor: "#226957",
                fontFamily: "Montserrat",
                minWidth: isMobile ? "150px" : "200px",
              }}
              onClick={handleBuynow}
            >
              Buy Now
            </Button>
          </Box>
        </Grid>
      </Grid>
      <CustomSnackbar
        open={snackbarOpen}
        setOpen={setSnackbarOpen}
        message={snackbarMessage}
        severity={snackbarSeverity}
      />
    </Container>
  );
}
