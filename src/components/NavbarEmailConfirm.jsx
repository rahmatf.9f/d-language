import React from 'react'
import { AppBar, Toolbar, Typography } from "@mui/material"

const NavbarEmailConfirm = () => {
    const appBar = {
        backgroundColor: '#ffffff',
        boxShadow: 'none'
    };
  return (
    <div>
       <AppBar position='static' style={appBar}>
        <Toolbar>
            <img src="/Images/logo/logo.png" alt="logo" style={{ marginLeft:'60px', marginTop:'18px'}} />
            <Typography variant="h5" component='div' sx={{ flexGrow: 1 }}  style={{color:'#000000', marginLeft:'10px', marginTop:'28.5px', font:'Montserrat'}}>
                Language
            </Typography>
        </Toolbar>
    </AppBar>
    </div>
  )
}

export default NavbarEmailConfirm
