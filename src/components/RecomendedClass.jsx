import { Box, Typography, Grid,useMediaQuery } from '@mui/material'
import React, { useState } from 'react';
import Cards from './Cards';
import LoadingScreen from './loading';
import useAsycn from '../hooks/useAsync';
const RecomendedClass = () => {

  const [NavLink] = useState('detailclass');
  const isMobile = useMediaQuery("(max-width:900px)");
  const ApiUrl = process.env.REACT_APP_GET_ALL_COURSE;
  const data = useAsycn(ApiUrl);
  return (
    <div>
        <Box sx={{
            display: 'flex',
            flexDirection: 'row',
            justifyContent: 'center',
            marginTop:'50px',
            marginBottom:'20px'
        }}>
        <Typography sx={{
            color:'#226957',
            fontSize:'18px',
            fontWeight:'550'
        }}>Recomended Class
        </Typography>
        </Box>
        <Box sx={{ 
                    display: 'flex',
                    flexDirection: 'row',
                    justifyContent: isMobile ? 'center' : 'flex-start',
                    marginX: isMobile ? "25px" : "50px"
          }}>
        <Grid container spacing={2} sx={{ marginBottom:'50px',display:'flex',justifyContent:'center' }}>
          {data.length > 0 ? (
            data.slice(0,6).map((item) => (
              <Grid item xs={12} sm={6} md={4} key={item.id_class} sx={{ display: 'flex', justifyContent: 'center' }}>
                <Cards item={item} link={NavLink} />
              </Grid>
            ))
          ):(
            <LoadingScreen/>
          )}
        </Grid>
        </Box>
    </div>
  )
}

export default RecomendedClass