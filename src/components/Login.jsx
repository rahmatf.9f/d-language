import { Grid, Paper, TextField, Typography, Button } from "@mui/material"

const Login = () => {
    const paperStyle = {
        display: 'flex',
        flexDirection: 'column',
        position: 'absolute',
        width: '616px',
        height: '450px',
        left: '332px',
        top: '146px',
        boxShadow: 'none'
    };
    const welcomeStyle = {
        color:'#226957',
        fontSize:'24px',
        font:'Montserrat',
        marginBottom: '16px'
    };
    const logInFirstStyle = {
        color:'#4F4F4F',
        fontSize:'16px',
        font:'Montserrat',
        marginBottom: '30px'
    };
      
  return (
    <div>
      <Grid>
        <Paper style={paperStyle} > 
            <Typography variant="h4" style={welcomeStyle}>Welcome Back!</Typography>
            <Typography variant="caption" style={logInFirstStyle}>Please login first</Typography>
            <form>
                <TextField fullWidth label='Email' style={{ margin: '12px 0' }} />
                <TextField fullWidth label='Password' type="password" style={{ margin: '12px 0' }} />
            </form>
            <Typography style={{ marginTop: '12px', fontSize:'16px', font:'Montserrat'}}>Forgot Password? <a href="/">Click Here</a></Typography>
            <div style={{ display: 'flex', justifyContent: 'flex-end', marginTop:'40px' }}>
                <Button style={{background:'#226957', width:'140px', height:'38px', padding:'10px', borderRadius:'8px', color:'#ffffff'}}>Login</Button>
            </div>
            <div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center', marginTop:'60px'}}>
                <Typography>Dont have account <a href="/">Sign Up Here</a></Typography>
            </div>
        </Paper>
      </Grid>
    </div>
  )
}

export default Login
