import { Box, Typography, Grid, useMediaQuery } from "@mui/material";
import React from "react";

const Benefit = () => {
  const isMobile = useMediaQuery("(max-width: 600px)"); // Mengubah min-width menjadi max-width
  return (
    <Box>
      <Grid
        sx={{
          backgroundColor: "#EA9E1F",
          height: isMobile ? "280px" : "410px",
          display: "flex",
          justifyContent: "center",
        }}
      >
        <Grid item xs={8}>
          <Box sx={{ padding: isMobile ? "30px 20px" : "80px" }}>
            <Typography
              style={{
                color: "#FFFFFF",
                fontSize: isMobile ? "24px" : "32px",
                marginBottom: isMobile ? "10px" : "20px",
              }}
            >
              Gets your best benefit
            </Typography>
            <Typography
              style={{ color: "#FFFFFF", fontSize: isMobile ? "12px" : "14px" }}
            >
              Sunt ex elit ullamco laborum id nulla magna nulla ea pariatur
              ipsum. Reprehenderit in id ipsum cillum nisi sit sunt ullamco
              fugiat voluptate sint ipsum nostrud ex. Occaecat aliqua consequat
              deserunt dolor nulla deserunt dolor duis. Laborum sit eiusmod
              tempor ex amet amet do deserunt aliqua cillum labore sit nisi
              cillum. Sunt ex elit ullamco laborum id nulla magna nulla ea pariatur
              ipsum. Reprehenderit in id ipsum cillum nisi sit sunt ullamco
              fugiat voluptate sint ipsum nostrud ex.
            </Typography>
          </Box>
        </Grid>
        <Grid item xs={4} sx={{ marginRight: isMobile ? "0" : "50px" }}>
          <Box sx={{ display: "flex" }}>
            <img
              src="/Images/tutorMen.png"
              alt=""
              style={{
                marginTop: isMobile ? "70px" : "50px",
                height: isMobile ? "135px" : "269px",
                width: isMobile ? "122px" : "245px",
              }}
            />
            <img
              src="/Images/tutorGirl.png"
              alt=""
              style={{
                marginTop: isMobile ? "90px" : "80px",
                marginLeft: isMobile ? "-90px" : "-150px",
                height: isMobile ? "135px" : "269px",
                width: isMobile ? "122px" : "245px",
              }}
            />
          </Box>
        </Grid>
      </Grid>
    </Box>
  );
};

export default Benefit;
