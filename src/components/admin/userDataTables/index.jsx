import React, { useState } from "react";
import {Paper,Table,TableBody,TableCell,TableContainer,TableHead,TablePagination,TableRow,Button,Dialog,DialogTitle,DialogContent,DialogActions,Typography} from "@mui/material";
import useMediaQuery from "@mui/material/useMediaQuery";
import ChangeCircleIcon from '@mui/icons-material/ChangeCircle';
import axios from "axios";
import CustomSnackbar from "../../CustomSnackbar";
import { grey } from "@mui/material/colors";
import SettingsIcon from '@mui/icons-material/Settings';
export default function UserDataTable({ columns, rows,handleOpenModal }) {
  const [page, setPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(5);
  const isMobile = useMediaQuery("(max-width: 900px)");
    const [open,setOpen] = useState(false);
    const [selectedUser,SetSelectedUser] = useState(null);
    const [snackbarOpen, setSnackbarOpen] = useState(false);
    const [snackbarMessage, setSnackbarMessage] = useState("");
    const [snackbarSeverity, setSnackbarSeverity] = useState("success");
    const handleChangePage = (event, newPage) => {
      setPage(newPage);
    };
    
      const handleDetailsClick = (user) => {
        SetSelectedUser(user);
        setOpen(true);
      };

      const handleStatusChange = () =>{
        const data = {
            user_Id: selectedUser.User_Id,
            name: selectedUser.Name,
            email: selectedUser.Email,
            role: selectedUser.Role,
            isActive: selectedUser.IsActive === "Active" ? true : false,
          }

          axios.put(process.env.REACT_APP_UPDATE_STATUS_USER, data)
          .then((response) => {
            setSnackbarOpen(true);
            setSnackbarMessage("Account Status Has Changed!");
            setSnackbarSeverity("success");
            setTimeout(() => window.location.reload(), 3000);
          })
          .catch((error) => {
              alert('Error: ' + error);
          })
      }

      const handleCloseModal = () =>{
        setOpen(false);
      }
    
      const handleChangeRowsPerPage = (event) => {
        setRowsPerPage(+event.target.value);
        setPage(0);
      };

    return(
    <Paper sx={{ width: "100%", overflow: "hidden" }}>
      <TableContainer
        sx={{
          maxHeight: isMobile ? 300 : 500,
          overflow: isMobile ? "auto" : "initial",
        }}
      >
        <Table stickyHeader aria-label="sticky table">
          <TableHead>
            <TableRow>
              {columns.map((column) => (
                <TableCell
                  align="center"
                  sx={{
                    fontFamily: "Montserrat",
                    fontSize: "20px",
                    backgroundColor: "#226957",
                    color: "#FFF",
                  }}
                  key={column.field}
                >
                  {column.headerName}
                </TableCell>
              ))}
                <TableCell
                  align="center"
                  sx={{
                    fontFamily: "Montserrat",
                    fontSize: "20px",
                    backgroundColor: "#226957",
                    color: "#FFF",
                  }}
                >
                  Action
                </TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {rows
              .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
              .map((row, index) => (
                <TableRow
                  hover
                  key={row.id}
                  sx={{
                    backgroundColor:
                      index % 2 === 0 ? "rgba(234, 158, 31, 0.2)" : "inherit",
                    colorAdjust: "exact",
                  }}
                >
                  {columns.map((column) => (
                    <TableCell
                      align="center"
                      sx={{ fontFamily: "Montserrat", fontSize: "16px" }}
                      key={column.field}
                    >
                      {row[column.field]}
                    </TableCell>
                  ))}
                    <TableCell align="center">
                    <Button
                          variant="text"
                          sx={{color:grey[500], width: "50px" }}
                          onClick={() => handleOpenModal(row)}
                        >
                          <SettingsIcon/>
                        </Button>
                      <Button
                        variant="text"
                        sx={{ color: "#EA9E1F", width: "50px" }}
                        onClick={() => handleDetailsClick(row)}
                      >
                        <ChangeCircleIcon/>
                      </Button>
                    </TableCell>
                </TableRow>
              ))
            }
          </TableBody>
        </Table>
      </TableContainer>
      <TablePagination
        rowsPerPageOptions={[5, 10]}
        component="div"
        count={rows.length}
        rowsPerPage={rowsPerPage}
        page={page}
        onPageChange={handleChangePage}
        onRowsPerPageChange={handleChangeRowsPerPage}
      />
      <Dialog open={open} onClose={handleCloseModal}>
        <DialogTitle>Change Status ?</DialogTitle>
        <DialogContent>
          <Typography>
            User Name: {selectedUser?.Name}
          </Typography>
          <Typography>
            Current Status: {selectedUser?.IsActive}
          </Typography>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleCloseModal} variant="text" sx={{ color: "#EA9E1F" }}>Cancel</Button>
          <Button onClick={handleStatusChange} variant="contained" sx={{ backgroundColor: "#226957" }} >Change Status</Button>
        </DialogActions>
      </Dialog>

      <CustomSnackbar open={snackbarOpen} message={snackbarMessage} severity={snackbarSeverity}/>
    </Paper>
    )
}