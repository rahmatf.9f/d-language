import * as React from 'react';
import PropTypes from 'prop-types';
import AppBar from '@mui/material/AppBar';
import Avatar from '@mui/material/Avatar';
import Grid from '@mui/material/Grid';
import IconButton from '@mui/material/IconButton';
import Link from '@mui/material/Link';
import MenuIcon from '@mui/icons-material/Menu';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import { Box, Button, Popover,List,ListSubheader,ListItemButton,ListItemIcon,ListItemText } from '@mui/material';
import { useState,useContext } from 'react';
import LogoutIcon from '@mui/icons-material/Logout';
import { useHistory } from 'react-router-dom/cjs/react-router-dom';
import { UserLogin } from '../../../context/UserLogin';
import LogoutConfirmationModal from '../../user/modalLogout';
import AccountCircleIcon from '@mui/icons-material/AccountCircle';

const lightColor = 'rgba(255, 255, 255, 0.7)';
function Header(props) {
  const history = useHistory();
  const { onDrawerToggle, Title } = props;
  const [anchorPop, setAnchorPop] = useState(null);
  const {setLoggedIn} = useContext(UserLogin);
  const handlePopOverOpen = (event) => {
    setAnchorPop(event.currentTarget)
  }
  const handlePopOverClose = (event) =>{
    setAnchorPop(null)
  }
  const [showLogoutModal, setShowLogoutModal] = useState(false);
  const handleLogout = () => {
    // Clear the login-related data from localStorage
    localStorage.removeItem("loggedIn");
    localStorage.removeItem("userRole");
    localStorage.removeItem("user_id");

    // Set the login state to false
    setLoggedIn(false);

    // Redirect to the login page (or any other desired location after logout)
    history.push("/");
  };
  const handlemodalLogout = () => {
    setShowLogoutModal(true);
  };
  const open = Boolean(anchorPop);
  const id = open ? 'simple-popover' : undefined;

  return (
    <React.Fragment>
      <AppBar sx={{ backgroundColor:'#226957' }} position="sticky" elevation={0}>
        <Toolbar>
          <Grid container spacing={1} alignItems="center">
            <Grid sx={{ display: { sm: 'none', xs: 'block' } }} item>
              <IconButton
                color="inherit"
                aria-label="open drawer"
                onClick={onDrawerToggle}
                edge="start"
              >
                <MenuIcon />
              </IconButton>
            </Grid>
            <Grid item xs />
            <Grid item>
              <Link
                href="/"
                variant="body2"
                sx={{
                  textDecoration: 'none',
                  color: lightColor,
                  '&:hover': {
                    color: 'common.white',
                  },
                }}
                rel="noopener noreferrer"
                target="_blank"
              >
                Go to Landing Page
              </Link>
            </Grid>
            <Grid item>
              <IconButton color="inherit" sx={{ p: 0.5 }} onClick={handlePopOverOpen}>
                <Avatar src="/Images/logo.png" alt="My Avatar" />
              </IconButton>
            </Grid>
          </Grid>
        </Toolbar>
      </AppBar>
      <Popover
        id={id}
        open={open}
        anchorEl={anchorPop}
        onClose={handlePopOverClose}
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'center',
        }}
        transformOrigin={{
          vertical: 'top',
          horizontal: 'center',
        }}
      >
    <List
      sx={{ width: '100%', maxWidth: 360, bgcolor: 'background.paper' }}
      component="nav"
      aria-labelledby="nested-list-subheader"
      subheader={
        <ListSubheader component="div" id="nested-list-subheader">
          Account Detail
        </ListSubheader>
      }
    >
      <ListItemButton>
        <ListItemIcon>
          <AccountCircleIcon />
        </ListItemIcon>
        <ListItemText primary="Account" />
      </ListItemButton>
      <ListItemButton onClick={handlemodalLogout}>
        <ListItemIcon>
          <LogoutIcon />
        </ListItemIcon>
        <ListItemText primary="Logout" />
      </ListItemButton>
    </List>
      </Popover>
      <AppBar
        component="div"
        position="static"
        elevation={0}
        sx={{ zIndex: 0,backgroundColor:'#226957' }}
      >
        <Toolbar>
          <Grid container alignItems="center" spacing={1}>
            <Grid item xs>
              <Typography color="inherit" variant="h5" component="h1">
                {Title}
              </Typography>
            </Grid>
          </Grid>
        </Toolbar>
      </AppBar>
      <LogoutConfirmationModal 
        open={showLogoutModal}
        handleClose={() => setShowLogoutModal(false)}
        handleLogout={handleLogout} />
    </React.Fragment>
  );
}

Header.propTypes = {
  onDrawerToggle: PropTypes.func.isRequired,
};

export default Header;