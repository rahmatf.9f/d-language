import * as React from 'react';
import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import { Box } from '@mui/material';

export default function AlertDialog({AlertDialog,open,close,handleSubmit}) {
    
    return(
        <Box>
            <Dialog
        open={open}
        onClose={close}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle id="alert-dialog-title">
          {AlertDialog.Title}
        </DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-description">
            {AlertDialog.content}
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={close}>No</Button>
          <Button onClick={handleSubmit} autoFocus>
            Yes
          </Button>
        </DialogActions>
      </Dialog>
        </Box>
    )
}