import React, { useState } from "react";
import {
  Typography,
  Box,
  Grid,
  useMediaQuery,
} from "@mui/material";
import useAsycn from "../hooks/useAsync";
import FlagCards from "./homepage/flagCard";
import LoadingScreen from "./loading";

const AvailableLanguage = () => {
  const isMobile = useMediaQuery("(max-width:600px)");
  const [NavLink] = useState("menulist");
  const Apiurl = process.env.REACT_APP_GET_LANGUAGE_CATEGORIES;
  const DataFlags = useAsycn(Apiurl);
  return (
    <Box sx={{ marginY: "50px" }}>
      <Box
        sx={{
          display: "flex",
          textAlign: "center",
          justifyContent: "center",
        }}
      >
        <Typography
          style={{
            fontSize: isMobile ? "20px" : "24px",
            color: "#226957",
          }}
        >
          Available Language Course
        </Typography>
      </Box>
      <Grid
        container
        rowSpacing={isMobile ? 3 : 7}
        columnSpacing={isMobile ? 1 : "-200px"}
        sx={{
          display: "flex",
          textAlign: "center",
          justifyContent: "center",
          marginTop: "10px",
          marginLeft: isMobile ? "0" : "130px",
        }}
      >
        {DataFlags.length > 0 ? (
          DataFlags.map((flag) => (
            <Grid item xs={isMobile ? 6 : 3} key={flag.id_language} sx={{ display: 'flex', justifyContent: 'center' }}>
              <FlagCards item={flag} link={NavLink} />
            </Grid>
          ))
        ) : (
          <LoadingScreen />
        )}
      </Grid>
    </Box>
  );
};

export default AvailableLanguage;
