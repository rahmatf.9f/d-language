import React from 'react'
import { Card, CardMedia, CardContent, Typography,CardActionArea,Box} from '@mui/material';
import { useHistory } from "react-router-dom";
const Cards = ({ item,link }) => {
  const history = useHistory();
  const handleButtonClick = () => {
    const itemId = item.id_class;
    history.push(`/${link}/${itemId}`);
  };
  return (
<Card sx={{ maxWidth: 400, minHeight:350}}>
      <CardActionArea onClick={handleButtonClick}>
        <CardMedia
          component="img"
          height="auto"
          width="100%"
          image={item.img}
          alt={item.name}
        />
        <CardContent sx={{ height:'auto' }}>
          <Box sx={{ marginBottom:'10px' }}>
          <Typography gutterBottom variant="body1" component="div" color="text.secondary">
            {item.name}
          </Typography>
          <Typography variant="body2" color="text.primary" sx={{ fontSize:'20px' }}>
            {item.description}
          </Typography>
          </Box>
          <Typography variant="body1" color="#226957" sx={{ fontWeight:500 }}>
            IDR {item.price}
          </Typography>
        </CardContent>
      </CardActionArea>
    </Card>
  )
}

export default Cards