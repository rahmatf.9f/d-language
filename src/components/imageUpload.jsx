import React, { useState } from 'react';
import { Button, Input, Card, CardMedia } from '@mui/material';
import {storage} from '../context/firebase';
import { ref, uploadBytes,getDownloadURL } from 'firebase/storage';
import {v4} from 'uuid'
const ImageUpload = ({setImageURL}) => {
  const [selectedImage, setSelectedImage] = useState(null);
  const [previewImage, setPreviewImage] = useState(null);

  const handleImageChange = (e) => {
    const file = e.target.files[0];
    setSelectedImage(file);
    setPreviewImage(URL.createObjectURL(file));
  };

  return (
    <div>
      <Input
        type="file"
        inputProps={{
          accept: 'image/*'
        }}
        onChange={handleImageChange}
        style={{ display: 'none' }}
        id="image-input"
      />
      <label htmlFor="image-input">
        <Button component="span">
          Select Image
        </Button>
      </label>

      {selectedImage && (
        <Card sx={{ maxWidth: 300, margin: '20px auto' }}>
          <CardMedia
            component="img"
            height="200"
            image={previewImage}
            alt="Selected Image"
          />
        </Card>
      )}

      {selectedImage && (
        <Button
          variant="contained"
          sx={{ backgroundColor: '#EA9E1F' }}
          onClick={() => {
            const imageRef = ref(storage, `images/paymenIcon/${selectedImage.name + v4()}`);

            uploadBytes(imageRef,selectedImage)
            .then(() =>{
              // when success
              getDownloadURL(imageRef)
              .then((url) => {
                setImageURL(url);
                alert("Image Upload to Firebase!");
              })
              .catch((error) => {
                console.error('Error getting download URL:', error);
              });
            })
          }}
        >
          Upload Image
        </Button>
      )}
    </div>
  );
};

export default ImageUpload;
