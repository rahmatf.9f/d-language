import React from "react";
import {
  Box,
  Grid,
  Typography,
  List,
  ListItem,
  IconButton,
  ListItemIcon,
  ListItemText,
  useMediaQuery,
  useTheme,
  AppBar,
  Toolbar,
  ListItemButton,
} from "@mui/material";
import {
  LocalPhone,
  Instagram,
  YouTube,
  Telegram,
  Email,
} from "@mui/icons-material";
import FiberManualRecordIcon from "@mui/icons-material/FiberManualRecord";
import useAsycn from "../hooks/useAsync";
import { useHistory } from "react-router-dom";

const Footer = () => {
  const data = [
    {
      id: 1,
      icon: <LocalPhone />,
    },
    {
      id: 2,
      icon: <Instagram />,
    },
    {
      id: 3,
      icon: <YouTube />,
    },
    {
      id: 4,
      icon: <Telegram />,
    },
    {
      id: 5,
      icon: <Email />,
    },
  ];

  const theme = useTheme();
  const isMobile = useMediaQuery(theme.breakpoints.down("sm"));
  const history = useHistory();
  const Apiurl = process.env.REACT_APP_GET_LANGUAGE_CATEGORIES;
  const DataFlags = useAsycn(Apiurl);
  const dataFlagsArray = Array.isArray(DataFlags) ? DataFlags : [];
  return (
    <AppBar
      position="static"
      sx={{
        backgroundColor: "#226957",
        top: "auto",
        bottom: 0,
        width: "100%",
      }}
    >
      <Toolbar
        sx={{
          marginX: isMobile ? 0 : "20px",
          marginY: "auto",
          paddingY: "30px",
        }}
        component="footer"
        square
        variant="outlined"
      >
        <Grid
          container
          spacing={isMobile ? 3 : 5}
          sx={{
            display: "flex",
            flexDirection: isMobile ? "column" : "row",
            alignContent: "center",
            justifyContent: isMobile ? "center" : "space-between",
          }}
        >
          <Grid item xs={12} md={4} textAlign={isMobile ? "center" : "left"}>
            <Typography sx={{ marginBottom: "10px" }}>About us</Typography>
            <Typography textAlign="justify">
              Lorem, ipsum dolor sit amet consectetur adipisicing elit.
              Dignissimos ipsam ut adipisci nobis corrupti, qui nostrum
              laudantium eveniet id distinctio harum, facilis ipsa? Temporibus
              recusandae enim laborum inventore facilis officia?
            </Typography>
          </Grid>

          <Grid item xs={12} md={4}>
            <Typography sx={{ textAlign: isMobile ? "center" : "center" }}>
              Product
            </Typography>
            <Grid
              container
              sx={{
                justifyContent: "center",
                marginLeft: isMobile ? "0" : "40px",
              }}
            >
              {dataFlagsArray.map((item) => {
                    const handleButtonClick = () => {
                      const itemName = item.name;
                      history.push(`/menulist/${itemName}`);
                    };
                return (
                  <Grid item xs={6} key={item.id} sx={{ marginBottom: "-15px" }}>
                    <ListItem>
                      <ListItemButton sx={{ padding:0 }} onClick={handleButtonClick}>
                      <ListItemIcon>
                        <FiberManualRecordIcon sx={{ height: "10px", width: "10px", color: "#FFFFFF" }} />
                      </ListItemIcon>
                      <ListItemText primary={item.name} />
                      </ListItemButton>
                    </ListItem>
                  </Grid>
                );
              })}

            </Grid>
          </Grid>

          <Grid item xs={12} md={4} textAlign="left">
            <Box sx={{ marginBottom: "14px" }}>
              <Typography
                sx={{ fontWeight: 500, fontSize: "16px", marginBottom: "10px" }}
              >
                Address
              </Typography>
              <Typography
                sx={{
                  color: "#FFFFFF",
                  fontWeight: 400,
                  fontSize: "14px",
                  width: "100%",
                }}
              >
                Id velit culpa amet et ipsum.Elit esse fugiat tempor ex. Laborum
                est minim pariatur nisi amet Lorem labore laborum incididunt.
              </Typography>
            </Box>
            <Box>
              <Typography
                sx={{
                  color: "#FFFFFF",
                  fontWeight: 500,
                  fontSize: "16px",
                  marginBottom: "10px",
                }}
              >
                Contact Us
              </Typography>
              <Box
                sx={{
                  display: "flex",
                  justifyContent: isMobile ? "center" : "flex-start",
                }}
              >
                {data.map((item) => {
                  return (
                    <Box
                      key={item.id}
                      sx={{
                        backgroundColor: "#FFFFFF",
                        borderRadius: "100%",
                        marginRight: "16px",
                      }}
                    >
                      <IconButton
                        sx={{
                          padding: "6px",
                          display: "flex",
                          color: "#FABC1D",
                          alignItems: "center",
                        }}
                      >
                        {item.icon}
                      </IconButton>
                    </Box>
                  );
                })}
              </Box>
            </Box>
          </Grid>
        </Grid>
      </Toolbar>
    </AppBar>
  );
};

export default Footer;
