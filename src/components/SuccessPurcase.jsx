import React, { useContext } from "react";
import { Typography, Button } from "@mui/material";
import HomeIcon from "@mui/icons-material/Home";
import ArrowRightAltIcon from "@mui/icons-material/ArrowRightAlt";
import { useHistory } from "react-router-dom";
import { UserLogin } from "../context/UserLogin";

const SuccessPurcase = () => {
  const {loggedIn} = useContext(UserLogin);
  const history = useHistory();
  return (
    <div>
      <div
        style={{
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
          flexDirection: "column",
        }}
      >
        <img
          src="/Images/transaction/succesTransaction.png"
          alt="."
          style={{ marginTop: "54px" }}
        />
        <Typography
          style={{ marginTop: "44px", color: "#226957", fontSize: "24px" }}
        >
          Purchase Successfully
        </Typography>
        <Typography
          style={{ marginTop: "8px", fontSize: "16px", color: "#4F4F4F" }}
        >
          Thanks to buy a course! See u in the class
        </Typography>
        <div>
          <Button
            startIcon={<HomeIcon />}
            style={{
              marginTop: "40px",
              background: "#EA9E1F",
              width: "200px",
              height: "50px",
              padding: "16px 24px",
              borderRadius: "6px",
              color: "#ffffff",
            }}
            onClick={() => history.push("/")}
          >
            <Typography style={{ fontSize: "15px" }}>Back to Home</Typography>
          </Button>
          <Button
            startIcon={<ArrowRightAltIcon />}
            style={{
              marginTop: "40px",
              marginLeft: "24px",
              background: "#226957",
              width: "200px",
              height: "50px",
              padding: "16px 24px",
              borderRadius: "6px",
              color: "#ffffff",
            }}
            onClick={
              loggedIn ? () => history.push("/invoice") : () => history.push("/login")
            }
          >
            <Typography style={{ fontSize: "15px" }}>Open Invoice</Typography>
          </Button>
        </div>
      </div>
    </div>
  );
};

export default SuccessPurcase;
