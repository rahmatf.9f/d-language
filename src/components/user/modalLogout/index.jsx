import React from "react";
import { Box,Modal,Typography,Button} from "@mui/material";

const LogoutConfirmationModal = ({ open, handleClose, handleLogout }) => {
    const okay = {
        color: "inherit",
        backgroundColor: "#226957",
        borderRadius: "8px",
        // padding: "10px 20px 10px 20px",
        height: "40px",
        width: "150px",
      };
      const cancel = {
        color: "inherit",
        backgroundColor: "#EA9E1F",
        borderRadius: "8px",
        // padding: "10px 20px 10px 20px",
        height: "40px",
        width: "150px",
      };
    return (
      <Modal open={open} onClose={handleClose}>
        <Box sx={{ position: "absolute", top: "50%", left: "50%", transform: "translate(-50%, -50%)", bgcolor: "white", p: 4, borderRadius: 4, outline: "none", boxShadow: 24 }}>
          <Typography variant="h5" sx={{ textAlign: "center",fontWeight: 700,fontFamily: "Montserrat" }}>
            Are you sure you want to logout?
          </Typography>
          <Box sx={{ display: "flex", justifyContent: "space-between", mt: 5 }}>
          <Button onClick={handleClose} style={cancel}>
                                <Typography
                  style={{
                    fontFamily: "Montserrat",
                    fontSize: "16px",
                    color: "white",
                    fontWeight: 500,
                  }}
                >
                  Cancel
                </Typography>
              </Button>
              <Button onClick={handleLogout} style={okay}>
                                <Typography
                  style={{
                    fontFamily: "Montserrat",
                    fontSize: "16px",
                    color: "white",
                    fontWeight: 500,
                  }}
                >
                  Logout
                </Typography>
              </Button>
          </Box>
        </Box>
      </Modal>
    );
  };

  export default LogoutConfirmationModal;