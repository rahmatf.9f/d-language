import React, { useState, useEffect } from "react";
import {
  Box,
  Typography,
  Checkbox,
  Card,
  CardContent,
  CardMedia,
  useTheme,
  useMediaQuery,
  IconButton,
  Grid,
} from "@mui/material";
import DeleteIcon from '@mui/icons-material/DeleteForever';
import AlertDialog from "../../AlertDialog";
import { red } from "@mui/material/colors";
import useDeleteItem from "../../../hooks/userDeleteItems";
import CustomSnackbar from "../../CustomSnackbar";
import useFormatDates from "../../../hooks/useFormatDates";
import useFormatPrice from "../../../hooks/useFormatPrice";

export default function CardlistCheck({
  item,
  handleCheckedItem,
  isCheckedAll,
}) {
  const theme = useTheme();
  const isMobile = useMediaQuery(theme.breakpoints.down("sm"));
  const [snackbarOpen, setSnackbarOpen] = useState(false);
  const [snackbarMessage, setSnackbarMessage] = useState("");
  const [snackbarSeverity, setSnackbarSeverity] = useState("success");

  const [isChecked, setIsChecked] = useState(false);
  const [open, setOpen] = React.useState(false);

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  useEffect(() => {
    setIsChecked(isCheckedAll);
  }, [isCheckedAll]);

  const handleCheckChange = () => {
    setIsChecked(!isChecked);
    handleCheckedItem(item, !isChecked);
  };
  const { deleteItem } = useDeleteItem();
  const handleDelete = () => {
    handleClose();

    const apiUrl = `${process.env.REACT_APP_DELETE_CART}?id=${item.id_cart}`;
    deleteItem(apiUrl);

    setSnackbarOpen(true);
    setSnackbarMessage("Item Deleted from your cart!");
    setSnackbarSeverity("success");
    setTimeout(()=>{  
      window.location.reload();
    }, 1000);
  };

  const DialogValue = [
    {
      Title: "Delete Class",
      content: "are You Sure You wanna Delete this Class ?",
    },
  ];

  return (
    <Box>
      <Card
        sx={{
          display: "flex",
          flexDirection: isMobile ? "column" : "row",
          width: "100%",
          height: isMobile ? "auto" : "170px",
          marginBottom: "20px",
        }}
      >
        <Checkbox
          checked={isChecked}
          onChange={handleCheckChange}
          sx={{
            marginTop: isMobile ? "10px" : 0,
            marginRight: isMobile ? 0 : "10px",
          }}
        />
        <CardMedia
          component="img"
          sx={{
            width: isMobile ? "100%" : "250px",
            height: isMobile ? "auto" : "auto",
          }}
          image={item.img}
          alt="Class Cover"
        />
        <Box sx={{ display: "flex", flexDirection: "column", width: "100%" }}>
          <CardContent>
            <Grid container  spacing={2}>
              <Grid item xs={10}>
                <Typography
                  variant="overline"
                  color="text.secondary"
                  component="div"
                  sx={{ textAlign: "left", fontFamily: "Montserrat" }}
                >
                  {item.name}
                </Typography>
                <Typography
                  variant="h5"
                  component="div"
                  sx={{
                    textAlign: "left",
                    fontFamily: "Montserrat",
                    fontWeight: "900",
                  }}
                >
                  {item.description}
                </Typography>
                <Typography
                  variant="body1"
                  color="text.secondary"
                  component="div"
                  sx={{
                    textAlign: "left",
                    fontSize: "16px",
                    fontFamily: "Montserrat",
                  }}
                >
                  Schedule: {useFormatDates(item.schedule_detail)}
                </Typography>
                <Typography
                  variant="subtitle1"
                  color="#EA9E1F"
                  component="div"
                  sx={{
                    textAlign: "left",
                    fontSize: "20px",
                    fontWeight: "600",
                    fontFamily: "Montserrat",
                  }}
                >
                  IDR {useFormatPrice(item.price)}
                </Typography>
              </Grid>

              <Grid item xs={2} sx={{ textAlign:'center' }} >
                <IconButton
                  onClick={handleClickOpen}
                  sx={{ alignSelf: "center",marginTop:'50px' }}
                  aria-label="Delete"
                >
                  <DeleteIcon sx={{ color:red[500] }}/>
                </IconButton>
              </Grid>
            </Grid>
          </CardContent>
        </Box>
      </Card>
      {DialogValue.map((value) => (
        <AlertDialog
          AlertDialog={value}
          open={open}
          close={handleClose}
          handleSubmit={handleDelete}
        />
      ))}
      <CustomSnackbar
        open={snackbarOpen}
        setOpen={setSnackbarOpen}
        message={snackbarMessage}
        severity={snackbarSeverity}
      />
    </Box>
  );
}
