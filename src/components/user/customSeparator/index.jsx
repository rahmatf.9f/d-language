import * as React from 'react';
import {Box, useTheme, useMediaQuery,Breadcrumbs,Typography,Link,} from '@mui/material';
import NavigateNextIcon from '@mui/icons-material/NavigateNext';
import { withRouter } from "react-router-dom";

const CustomSeparator = props => {
  const {
    history,
    location: { pathname }
  } = props;

  const pathnames = pathname.split("/").filter(x => x);

  return (
    <Breadcrumbs aria-label="breadcrumb" separator={<NavigateNextIcon fontSize="small" />} sx={{ marginBottom: 5 }}>
    {pathnames.length > 0 ? (
        <Link underline="hover" color="inherit" onClick={() => history.push("/")}>Home</Link>
      ) : (
        <Typography> Home </Typography>
      )}
      {pathnames.map((name, index) => {
        const routeTo = `/${pathnames.slice(0, index + 1).join("/")}`;
        const isLast = index === pathnames.length - 1;
        return isLast ? (
          <Typography color="#EA9E1F" key={name}>{name}</Typography>
        ) : (
          <Link underline="hover" color="#inherit" key={name} onClick={() => history.push(routeTo)}>
            {name}
          </Link>
        );
      })}
    </Breadcrumbs>
  );
};

export default withRouter(CustomSeparator);
