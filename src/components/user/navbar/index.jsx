import React, { useState } from "react";
import { useHistory } from "react-router-dom";
import {
  AppBar,
  Toolbar,
  Typography,
  IconButton,
  Box,
  MenuItem,
  useTheme,
  useMediaQuery,
  Drawer,
  List,
  ListItem,
  ListItemIcon,
  ListItemText,
  ListItemButton
} from "@mui/material";
import LogoutIcon from "@mui/icons-material/Logout";
import PersonIcon from "@mui/icons-material/Person";
import ShoppingCartIcon from "@mui/icons-material/ShoppingCart";
import MenuIcon from "@mui/icons-material/Menu";
import ClassIcon from "@mui/icons-material/Class";
import PaymentIcon from "@mui/icons-material/Payment";
import {UserLogin} from "../../../context/UserLogin";
import { useContext } from "react";
import LogoutConfirmationModal from "../modalLogout";
const NavbarAfterLogin = () => {

  const {setLoggedIn} = useContext(UserLogin);
  const appBar = {
    backgroundColor: "#ffffff",
  };
  const theme = useTheme();
  const isMobile = useMediaQuery(theme.breakpoints.down("md"));
  const history = useHistory();

  const [DrawerOpen, setDrawerOpen] = useState(false);
  const handleDrawerToggle = () => {
    setDrawerOpen(!DrawerOpen);
  };
  const drawerItems = [
    {
      label: "Cart",
      icon: <ShoppingCartIcon />,
      path: "/checkout",
    },
    {
      label: "My Class",
      icon: <ClassIcon />,
      path: "/myclass",
    },
    {
      label: "Invoice",
      icon: <PaymentIcon />,
      path: "/invoice",
    },
    {
      label: "My Account",
      icon: <PersonIcon />,
    },
    {
      label: "Logout",
      icon: <LogoutIcon />,
    },
  ];
  const [showLogoutModal, setShowLogoutModal] = useState(false);

  const handleLogout = () => {
    // Clear the login-related data from localStorage
    localStorage.removeItem("loggedIn");
    localStorage.removeItem("userRole");
    localStorage.removeItem("user_id");

    // Set the login state to false
    setLoggedIn(false);

    // Redirect to the login page (or any other desired location after logout)
    history.push("/");
  };
  const handlemodalLogout = () => {
    setShowLogoutModal(true);
  };

  const renderDrawerItems = () => {
    return (
      <List>
        {drawerItems.map((item, index) => (
          <ListItem key={index}>
            <ListItemButton onClick={() => history.push(`${item.path}`)}>
              <ListItemIcon>{item.icon}</ListItemIcon>
              <ListItemText primary={item.label} />
            </ListItemButton>
          </ListItem>
        ))}
      </List>
    );
  };

  return (
    <Box>
      <AppBar position="relative" style={appBar}>
        <Toolbar
          sx={{
            marginX: isMobile ? 0 : "100px",
            marginY: isMobile ? "0.6vh" : "auto",
          }}
        >
          <img src="/Images/logo.png" alt="logo" />
          <Typography
            variant="h5"
            noWrap
            component="a"
            href="/"
            // style={{
            //   color: "#000000",
            //   marginLeft: "10px",
            //   fontFamily: "Montserrat",
            // }}
            sx={{
              color: 'text.primary',
              textDecoration: 'none',
              fontFamily:'Montserrat',
              marginLeft: "10px",
            }}
          >
            Language
          </Typography>

          <Box sx={{ flexGrow: 1 }} />

          {isMobile ? (
            <IconButton onClick={handleDrawerToggle}>
              <MenuIcon />
            </IconButton>
          ) : (
            <Box
              sx={{ display: { xs: "none", md: "flex" }, alignItems: "center" }}
            >
              <IconButton
                style={{
                  color: "#226957",
                  width: "24px",
                  height: "24px",
                  marginRight: "30px",
                }}
                onClick={() => history.push("/checkout")}
              >
                <ShoppingCartIcon />
              </IconButton>
              <MenuItem onClick={() => history.push("/myclass")}>
                <Typography
                  style={{
                    fontFamily: "Montserrat",
                    fontSize: "16px",
                    color: "#226957",
                    fontWeight: 500,
                    marginRight: "30px",
                  }}
                >
                  My Class
                </Typography>
              </MenuItem>
              <MenuItem onClick={() => history.push("/invoice")}>
                <Typography
                  style={{
                    fontFamily: "Montserrat",
                    fontSize: "16px",
                    color: "#226957",
                    fontWeight: 500,
                    marginRight: "30px",
                  }}
                >
                  Invoice
                </Typography>
              </MenuItem>
              <Typography
                style={{
                  font: "Montserrat",
                  fontSize: "16px",
                  color: "black",
                  marginRight: "30px",
                }}
              >
                |
              </Typography>
              <IconButton
                style={{
                  color: "#226957",
                  width: "25px",
                  height: "25px",
                  marginRight: "30px",
                }}
              >
                <PersonIcon />
              </IconButton>
              <IconButton
                style={{
                  color: "red",
                  width: "20pxx",
                  height: "20pxx",
                  marginRight: "30px",
                }}
                onClick={handlemodalLogout}
              >
                <LogoutIcon />
                {/* logout */}
              </IconButton>
            </Box>
          )}
        </Toolbar>
      </AppBar>
      <Drawer anchor="right" open={DrawerOpen} onClose={handleDrawerToggle}>
        {renderDrawerItems()}
      </Drawer>
      <LogoutConfirmationModal
        open={showLogoutModal}
        handleClose={() => setShowLogoutModal(false)}
        handleLogout={handleLogout}
      />
    </Box>
  );
};

export default NavbarAfterLogin;
