import React, { useState } from "react";
import { Box, Modal, Typography, Button } from "@mui/material";

import ListPayment from "./listPayment";
import useAsycn from "../../../hooks/useAsync";

const modalStyle = {
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: 350,
  bgcolor: "background.paper",
  boxShadow: 24,
  p: 4,
};

export default function ModalPayment({
  open,
  handleClose,
  handlePayment,
  itemsList,
}) {
  
    const ApiUrl = process.env.REACT_APP_GET_PAYMENT_METHOD;
    const paymentlist = useAsycn(ApiUrl);

    // data output payment_method jika selected dan button clicked
    const [selectedPaymentMethod, setSelectedPaymentMethod] = useState(paymentlist[0]);

    const [select, setselect] = useState(0);
  
    //handle untuk output payment
    const handlePaymentButtonClick = () => {
    handlePayment(selectedPaymentMethod, itemsList);
    };
  // console.log(paymentlist);
  return (
    <Modal
      open={open}
      onClose={handleClose}
      aria-labelledby="modal-modal-title"
      aria-describedby="modal-modal-description"
    >
      <Box sx={modalStyle}>
        <Box
          sx={{
            display: "flex",
            justifyContent: "center",
            marginBottom: "5px",
          }}
        >
          <Typography
            id="modal-modal-title"
            variant="h6"
            component="h2"
            sx={{ fontSize: "20px", lineHeight: "30px" }}
          >
            Select Payment Method
          </Typography>
        </Box>
        <Box sx={{ width: "100%" }}>
          <ListPayment items={paymentlist} setIndex={setselect} index={select} setSelectedItem={setSelectedPaymentMethod}/>
          <Box
            sx={{
              display: "flex",
              justifyContent: "space-around",
              marginTop: "10px",
            }}
          >
            <Button
              variant="contained"
              sx={{
                marginRight: "10px",
                backgroundColor: "#EA9E1F",
                minWidth: "130px",
              }}
              onClick={handleClose}
            >
              Cancel
            </Button>
            <Button
              variant="contained"
              sx={{
                backgroundColor: "#226957",
                fontFamily: "Montserrat",
                minWidth: "130px",
              }}
              onClick={handlePaymentButtonClick}
            >
              Pay Now
            </Button>
          </Box>
        </Box>
      </Box>
    </Modal>
  );
}
