import React, { } from "react";
import {
  List,
  ListItemButton,
  ListItemContent,
  ListItemDecorator,
} from "@mui/joy";
import { Box } from "@mui/material";

export default function ListPayment({ items, setIndex,index,setSelectedItem  }) {

  return (
    <Box sx={{ width: "100%", maxWidth: 360, bgcolor: "background.paper" }}>
      <List
        component="nav"
        aria-label="main mailbox folders"
        sx={{
          "--ListItemDecorator-size": "70px",
          "--ListItemDecorator-marginY": "10px",
        }}
      >
        {items.map((item)=>(
                  <ListItemButton
                  selected={index === item.id_payment}
                  variant={index === item.id_payment ? "soft" : "plain"}
                  sx={ index === item.id_payment ? {backgroundColor:'#bdbdbd'} : undefined }
                  onClick={()=> {
                    setSelectedItem(item.id_payment)
                    setIndex(item.id_payment)
                  }}
                >
                  <ListItemDecorator>
                    <img src={item.img} alt={item.name} />
                  </ListItemDecorator>
                  <ListItemContent>{item.name}</ListItemContent>
                </ListItemButton>
        ))}
      </List>
    </Box>
  );
}
