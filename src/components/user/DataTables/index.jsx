import React, { useState } from "react";
import Paper from "@mui/material/Paper";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TablePagination from "@mui/material/TablePagination";
import TableRow from "@mui/material/TableRow";
import { Button } from "@mui/material";
import useMediaQuery from "@mui/material/useMediaQuery";
import { useHistory } from "react-router-dom/cjs/react-router-dom";

export default function DataTable({ columns, rows, includeActionColumn }) {
  const [page, setPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(10);
  const isMobile = useMediaQuery("(max-width: 900px)");
  const history = useHistory();
  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleDetailsClick = (id) => {
    history.push(`/invoice/details/${id}`);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(+event.target.value);
    setPage(0);
  };

  return (
    <Paper sx={{ width: "100%", overflow: "hidden" }}>
      <TableContainer
        sx={{
          maxHeight: isMobile ? 300 : 400,
          overflow: isMobile ? "auto" : "initial",
        }}
      >
        <Table stickyHeader aria-label="sticky table">
          <TableHead>
            <TableRow>
              {columns.map((column) => (
                <TableCell
                  align="center"
                  sx={{
                    fontFamily: "Montserrat",
                    fontSize: "20px",
                    backgroundColor: "#226957",
                    color: "#FFF",
                  }}
                  key={column.field}
                >
                  {column.headerName}
                </TableCell>
              ))}
              {/* include action column when in invoice page and not include action column in invoice details page */}
              {includeActionColumn && (
                <TableCell
                  align="center"
                  sx={{
                    fontFamily: "Montserrat",
                    fontSize: "20px",
                    backgroundColor: "#226957",
                    color: "#FFF",
                  }}
                >
                  Action
                </TableCell>
              )}
            </TableRow>
          </TableHead>
          <TableBody>
            {rows
              .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
              .map((row, index) => (
                <TableRow
                  hover
                  key={row.id}
                  sx={{
                    backgroundColor:
                      index % 2 === 0 ? "rgba(234, 158, 31, 0.2)" : "inherit",
                    colorAdjust: "exact",
                  }}
                >
                  {columns.map((column) => (
                    <TableCell
                      align="center"
                      sx={{ fontFamily: "Montserrat", fontSize: "16px" }}
                      key={column.field}
                    >
                      {column.field === 'total_price' || column.field === 'price'  ? `IDR ${row[column.field]}` : row[column.field]}
                    </TableCell>
                  ))}
                  {/* include action column when in invoice page and not include action column in invoice details page */}
                  {includeActionColumn && (
                    <TableCell align="center">
                      <Button
                        variant="contained"
                        sx={{ backgroundColor: "#EA9E1F", width: "180px" }}
                        onClick={() => handleDetailsClick(row['id_invoice'])}
                      >
                        Details
                      </Button>
                    </TableCell>
                  )}
                </TableRow>
              ))}
          </TableBody>
        </Table>
      </TableContainer>
      <TablePagination
        rowsPerPageOptions={[5, 10, 25]}
        component="div"
        count={rows.length}
        rowsPerPage={rowsPerPage}
        page={page}
        onPageChange={handleChangePage}
        onRowsPerPageChange={handleChangeRowsPerPage}
      />
    </Paper>
  );
}
