import React from 'react'
import { Grid, Paper, TextField, Typography, Button } from "@mui/material"
const Register = () => {
    const paperStyle = {
        display: 'flex',
        flexDirection: 'column',
        position: 'absolute',
        width: '616px',
        height: '482px',
        left: '332px',
        top: '146px',
        boxShadow: 'none'
    };
    const welcomeStyle = {
        color:'#226957',
        fontSize:'24px',
        marginBottom: '16px'
    };
    const dLanguageStyle = {
        color: '#EA9E1F',
        fontSize: '44px'
      };
    const logInFirstStyle = {
        color:'#4F4F4F',
        fontSize:'16px',
        font:'Montserrat',
        marginBottom: '30px'
    };
  return (
    <div style={{ height: '100vh'}}>
       <Grid>
        <Paper style={paperStyle} > 
        <Typography variant="h4" style={welcomeStyle}>
            Lets Join <span style={dLanguageStyle}>D'Language</span>    
        </Typography>
            <Typography variant="caption" style={logInFirstStyle}>Please register first</Typography>
            <form>
                <TextField fullWidth label='Name' style={{ margin: '12px 0'}} />
                <TextField fullWidth label='Email' style={{ margin: '12px 0' }} />
                <TextField fullWidth label='Password' type="password" style={{ margin: '12px 0' }} />
                <TextField fullWidth label='Confirm Password' type="password" style={{ margin: '12px 0' }} />
            </form>
            <div style={{ display: 'flex', justifyContent: 'flex-end', marginTop:'28px' }}>
                <Button style={{background:'#226957', width:'140px', height:'38px', padding:'10px', borderRadius:'8px', color:'#ffffff'}}>Sign Up</Button>
            </div>
            <div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center', marginTop:'60px'}}>
                <Typography>Have account <a href="/">Login here</a></Typography>
            </div>
        </Paper>
      </Grid>
    </div>
  )
}

export default Register
