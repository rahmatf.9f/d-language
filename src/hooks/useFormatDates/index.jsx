export default function useFormatDates(dateString) {
    const date = new Date(dateString);
    const options = { weekday: "long", day: "numeric", month: "long", year: "numeric" };
    return date.toLocaleDateString("en-US", options);
}