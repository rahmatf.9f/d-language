import { useState } from 'react';
import axios from 'axios';

const useDeleteItem = () => {
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(null);

  const deleteItem = (url) => {
    setLoading(true);
    setError(null);

    axios
      .delete(url)
      .then((response) => {
        setLoading(false);
        return "Item deleted successfully";
      })
      .catch((error) => {
        setLoading(false);
        setError(error);
        return "Error deleting item:" + error;
      });
  };

  return { deleteItem, loading, error };
};

export default useDeleteItem;
