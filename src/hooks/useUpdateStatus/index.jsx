import { useState } from 'react';
import axios from 'axios';

const useUpdateStatus = () => {
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(null);

  const updateStatus = async (itemsList) => {
    setLoading(true);
    setError(null);

    await axios.put(process.env.REACT_APP_UPDATE_STATUS_CART, itemsList)
      .then((response) => {
        setLoading(false);
        return response.data;
      })
      .catch((error) => {
        setLoading(false);
        setError(error);
        throw error;
      });
  };

  return { updateStatus, loading, error };
};

export default useUpdateStatus;
