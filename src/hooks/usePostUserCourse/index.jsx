import { useState } from 'react';
import axios from 'axios';

const usePostUserCourse = () => {
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(null);

  const postUserCourse = async (arrayUser_Course) => {
    setLoading(true);
    setError(null);

    await axios.post(process.env.REACT_APP_POST_USER_COURSE, arrayUser_Course)
      .then((response) => {
        setLoading(false);
        return response.data;
      })
      .catch((error) => {
        setLoading(false);
        setError(error);
        throw error;
      });
  };

  return { postUserCourse, loading, error };
};

export default usePostUserCourse;
