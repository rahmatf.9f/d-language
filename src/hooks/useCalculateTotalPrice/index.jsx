import React from 'react';

export default function useCalculateTotalPrice(data) {
    let total = 0;
    for(let i = 0; i < data.length; i++){
      total += data[i].price;
    }
    return total;
}