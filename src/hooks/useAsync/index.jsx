import React,{useState,useEffect} from "react";
import axios from "axios";

export default function useAsycn(urlApi) {
    const [data,setData] = useState({});

    useEffect(()=>{

        const callApi = () => {
            axios
                .get(`${urlApi}`, {})
                .then((res) => setData(res.data))
                .catch((err) => {
                console.log("error",err);
                });
            };

            callApi();
    },[urlApi])

    return data;
}