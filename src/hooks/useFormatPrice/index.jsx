import React,{useState,useEffect} from "react";

export default function useFormatPrice(price) {
    return price.toLocaleString("id-ID");
}